# README

The Points System is an app for tracking points and redeeming rewards.

###  Goal  ###



###  More Info  ###
See the `docs` directory.


### Setup ###

- create a virtual environment in your python root: `python3 -m virtualenv .venv`
- activate the virtualenv: `source .env-setup`
- install packages: `pip install -r requirements.txt`


### Deployment

#### k8s/helm

This application is fully deployable via helm, go to the `.devops/helm` directory and perform the following to deploy:

- configure the `values.yaml.prod` file per your needs
- ensure the `dx-blog-data` nfs share has been set up (pv and pvc will be created but the share itself needs to be created)
- deploy the chart: `helm install points points/ --values points/values.yaml.prod --create-namespace --namespace points --disable-openapi-validation`
  - update: `helm upgrade points points/ --values points/values.yaml.prod --namespace points --disable-openapi-validation`
  - uninstall: `helm uninstall -n points points`
  - NOTICE: after deploying the updated chart you need to rollout the change to the service impacted:
    + `kubectl rollout restart -n points deployment points-api ui`
    + `kubectl rollout restart -n points statefulset redis`
