#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id
from config.config import couchdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from logs.models import LogLevel, LogResponse, LogStatus, _db
from logs.utils import add_log_message
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
logs_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@logs_router.get("/all", response_model=List[LogResponse])
async def get_all(
    limit: int = Query(10, ge=1, le=9999, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all logs
    """

    # ~ logger.debug(f"- get all: current_user: {current_user}")
    db = await couchdb[_db.value]
    docs = []
    async for doc in db.find(selector={}, limit=limit, skip=skip):
        docs.append(fix_id(doc))

    return docs


@logs_router.get("/one/{_id}", response_model=LogResponse)
async def get_one(
    _id: str = Query(..., description="The id of the document to get"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    db = await couchdb[_db.value]

    return fix_id(await _get_or_404(_id, db))


@logs_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    level: LogLevel = Query(..., description="the log level"),
    status: LogStatus = Query(..., description="the status of the log item"),
    location: str = Query(
        ...,
        description="the location of the log (e.g. the component:function -> worker:process_activities)",
    ),
    message: str = Query(..., description="the log message"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    """

    return await add_log_message(
        level=level,
        status=status,
        location=location,
        message=message,
        creator=current_user.username,
    )


@logs_router.put("/", response_model=DocInfo)
async def update(
    _id: str = Query(..., description="The id of the document to get"),
    level: LogLevel = Query(None, description="the log level"),
    status: LogStatus = Query(None, description="the status of the log item"),
    location: str = Query(
        None,
        description="the location of the log (e.g. the component:function -> worker:process_activities)",
    ),
    message: str = Query(None, description="the log message"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update
    """

    db = await couchdb[_db.value]

    doc = await _get_or_404(_id, db)
    logger.debug(f"updating doc: {doc}")

    doc["level"] = level if level else doc["level"]
    doc["status"] = status if status else doc["status"]
    doc["location"] = location if location else doc["location"]
    doc["message"] = message if message else doc["message"]

    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    # ~ logger.debug(f"doc before save: {doc}")
    await doc.save()

    return await doc.info()


@logs_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Hard Delete by id
    """

    db = await couchdb[_db.value]

    resp = DocInfo(ok="unknown", id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = "ok"
        resp.msg = "deleted"
    else:
        resp.ok = "fail"
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
