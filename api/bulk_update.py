#
# bulk update helper
#
import asyncio

from aiocouch import CouchDB


async def main_with():
    async with CouchDB("http://db:5984", user="", password="") as couchdb:  # nosec
        db = await couchdb["points"]

        async for doc in db.docs():
            print(f"- doc: {doc['_id']}")
            if "quantity" in doc:
                print(f"-  ==> has quantity ({doc['quantity']}) - SKIPPING")
            else:
                print("- setting quantity...")
                doc["quantity"] = 1
                await doc.save()
                sr = await doc.info()
                print(f"-  ==> save response: {sr}")


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main_with())
