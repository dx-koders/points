# test_main.py
#
# @NOTES
#  - we can now run with: `pytest`, previously we needed to call as follows:
#    + run with: python -m pytest (as `pytest` does not work - path issues)
#      - normal: python -m pytest -ra --tb=short --no-header
#    + if you wanted detailed debug info (e.g. print statement output) you need to call with the `-rP` params (e.g. `python -m pytest -rP`).
#  - you need to set several envvars to run this test (see os.getenv) you can pass these in when running the test (e.g. $ TEST_APP_USER_USERNAME=tester python -m pytest) or (recommended approach) is to set them in the .env file so they will be set in the app container
#  - if you are sending 'params' in a payload they need to be encoded but you cannot do jsonable_encode() or json.dumps() on the entire object, you must do it at the individual property level (e.g. `"enabled": True`)
#  - if you want tests to utilize direct ASGI access you need to set app=app
#  - tests are in the following order:
#    1. order of need (e.g. you need an Activity before you can have Related Items)
#    2. alphabetical (i.e. Activities* before Users*, etc.)
# @LINEAGE: 2025-02-07: sync with style and info from radiant
#
# ADDITIONAL ITEMS TO TEST:
#  - use Task with available_units to ensure available_units decrements
#  - use Task with available_units until all unites are gone to ensure you can't go beyond limit
#  - complete a task to ensure user gets points
#  - redeem Reward to ensure user points decrements
#  - redeem Reward above # of points to ensure it is blocked
#

import logging
import os
import pytest
import uuid
from httpx import AsyncClient

# from main import app

from user_messages.models import UserMessageCategory, UserMessagePriority
from tasks.models import Kind
from points.models import PointStatus, PointType
from stashes.models import StashEditType
from config.config import APP_LOGLEVEL

# ~ PATH_SERVER = "http://mitm:8080"
PATH_SERVER = "http://api:8000"
PATH_BASE = "/api"
PATH_APP = f"{PATH_BASE}/v2"

logger = logging.getLogger("default")
logger.setLevel(logging.getLevelName(APP_LOGLEVEL))

# async_client_app is the app (context) sent with the httpx AsyncClient: this should either be app or None:
# - None: use this if you do not want to use direct ASGI access (which is needed if you want to proxy traffic through
#         mitmproxy or sniff the tcp traffic.
# - app: (e.g. from main import app) this is used if you want to send http traffic to the app directly via ASGI which
#         is faster and more efficient but does not allow typical sniffing/debug at the http/tcp layer.
async_client_app = None
# ~ async_client_app = app   # do not forget to ensure app is imported: `from main import app`

runstamp = uuid.uuid4().hex
_app_user_username = os.getenv("TEST_APP_USER_USERNAME")
_app_user_password = os.getenv("TEST_APP_USER_PASSWORD")
_auth_user_username = os.getenv("TEST_AUTH_USER_USERNAME")
_auth_user_password = os.getenv("TEST_AUTH_USER_PASSWORD")

_dispatcher_email_server_url = os.getenv("TEST_DISPATCHER_EMAIL_SERVER_URL")
_dispatcher_email_server_port = os.getenv("TEST_DISPATCHER_EMAIL_SERVER_PORT")
_dispatcher_email_server_username = os.getenv("TEST_DISPATCHER_EMAIL_SERVER_USERNAME")
_dispatcher_email_server_password = os.getenv("TEST_DISPATCHER_EMAIL_SERVER_PASSWORD")
_dispatcher_email_to = os.getenv("TEST_DISPATCHER_EMAIL_TO")

app_version = {"status": "OK", "version": "3.5.0", "created": "2021-02-22", "modified": "2025-02-07"}
# app_user* is the user used when executing tests
app_user_for_update_username = "tester4update"
app_user = {
    "username": _app_user_username,
    "password": _app_user_password,
    "grant_type": "password",
}
app_user_for_update = {
    "display_name": "updated by pytest",
    "avatar": "https://dradux.com/avatar/tester4update",
    "note": f"updated by pytest: runstamp={runstamp}",
    "enabled": True,
}
# user that tests authentication (as opposed to user used for running tests)
auth_user = {
    "username": _auth_user_username,
    "password": _auth_user_password,
    "grant_type": "password",
}
# test account: used by test users for testing.
account_1 = {
    "name": "pytest",
    "note": "test account created for pytest testing",
    "enabled": True,
}
account_2 = {
    "name": "pytest2",
    "note": "another test account created for pytest testing",
    "enabled": False,
}
task_1 = {
    "account": "<<this will be added before usage>>",
    "name": "PyTest Test",
    "description": "This is a test Task created by pytest",
    "value": 1,
    "icon": "bi bi-music-note",
    "enabled": False,
    "available_units": 100,
    "kind": Kind.general.value,
}
reward_1 = {
    "account": "<<this will be added before usage>>",
    "name": "PyTest Test",
    "description": "This is a test Reward created by pytest",
    "value": 1,
    "icon": "bi bi-music-note",
    "enabled": False,
    "available_units": 100,
    "kind": Kind.general.value,
}
point_1 = {
    "account": "<<this will be added before usage>>",
    "status": PointStatus.created.value,
    "point_type": PointType.tasks.value,
    "point_key": "<<this will be added before usage>>",
    "note": "a test point (task) created by pytest",
    "message": "a test point (task) created by pytest",
    "quantity": 1,
}
point_2 = {
    "account": "<<this will be added before usage>>",
    "status": PointStatus.created.value,
    "point_type": PointType.rewards.value,
    "point_key": "<<this will be added before usage>>",
    "note": "a test point (reward) created by pytest",
    "message": "a test point (reward) created by pytest",
    "quantity": 1,
}
user_message_1 = {
    "to_user_id": app_user["username"],
    "to_user_account": account_1["name"],
    "category": UserMessageCategory.system.value,
    "priority": UserMessagePriority.high.value,
    "subject": "PyTest Test Message #1",
    "message": "This is a test message created by pytest",
}
stash_1 = {
    "account": "<<this will be added before usage>>",
    # ~ "username": "",
    "icon": "bi bi-gem",
    "label": "Test 1",
    "note": "a test stash created by pytest that should fail",
    "amount": 100,
}
stash_2 = {
    "account": "<<this will be added before usage>>",
    # ~ "username": "",
    "icon": "bi bi-gem",
    "label": "Test 2",
    "note": "a test stash created by pytest",
    "amount": 1,
}

# _cleanup_pause: minimum 2 seconds, might need more if test_posts_count is failing
#   as the post is likely not persisting via worker fast enough.
_cleanup_pause = 2

account_1_id = None  # to store account #1 id
account_2_id = None  # to store account #2 id
user_message_1_id = None  # to store user message #1 id
test_message_id = None  # to store message id
task_1_id = None  # to store task #1 id
reward_1_id = None  # to store task #1 id
point_1_id = None  # to store point #1 (task) id
point_2_id = None  # to store point #2 (reward) id
stash_1_id = None  # to store stash #1 (insufficient funds) id
stash_2_id = None  # to store stash #2 (good) id

headers_base = {"content-type": "application/json", "accept": "application/json"}
headers_text = {"content-type": "text/plain", "accept": "application/json"}
headers_form = {"content-type": "application/x-www-form-urlencoded", "accept": "application/json"}


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
@pytest.fixture(scope="module")
async def get_auth_token():
    """
    Get Authentication Token

    @NOTES
    - the token is used throughout the remainder of the tests
    """

    logger.info(
        f"""
---------------------------------
- SERVER: {PATH_SERVER}
- BASE PATH: {PATH_APP}
- Test User: {app_user["username"]}
---------------------------------
"""
    )

    logger.info("getting auth token...")

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/auth/token",
            data=app_user,
            headers={"content-type": "application/x-www-form-urlencoded"},
        )

    logger.info(f"response: {response.text}")
    if response and response.json():
        token = response.json()["access_token"]
        return token
    else:
        pytest.exit(99)


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def setup_user_accounts_test(get_auth_token):
    """
    Add 'accounts' to the test users to allow the users appropriate access.

    do we add the 'default' or make a new one for testing? testing I think is better.
    """

    global account_1_id

    logger.info(f"get test account with name={account_1['name']}")

    # check for 'test' account, return it if so, otherwise create it!
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/accounts/{account_1['name']}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f" response: {response}")

    if response.status_code == 200:
        logger.info(" test Account exists...")
        account_1_id = account_1["name"]
    else:
        logger.info(" test Account not found, create it...")
        # create 'test' account
        async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
            response = await ac.post(
                f"{PATH_APP}/accounts/",
                params=account_1,
                headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
            )

        assert response.status_code == 201
        assert response.json()["ok"] is True
        assert len(response.json()["id"]) > 0
        # set account_1_id so we can use it later.
        account_1_id = response.json()["id"]

    # add app_user user to 'test' account
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/users/{app_user['username']}",
            json={"accounts": [{"id_": "pytest", "role": "admin"}]},
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True

    # add app_user_for_update user to 'test' account
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/users/{app_user_for_update_username}",
            json={"accounts": [{"id_": "pytest", "role": "member"}]},
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def authentication_basic_test():
    """
    Authentication
    - notes:
      + we use this to test auth (which is already 'tested' via our 'get_auth_token' above
          but we want a true test of auth plus we want to authenticate as the tester4update
          user in case the app side of this account is not yet set up.
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/auth/token",
            data=auth_user,
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
    assert response.status_code == 200
    assert response.json()["token_type"] == "bearer"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def accounts_add_test(get_auth_token):
    """
    Account: add
    """

    global account_2_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/accounts/",
            params=account_2,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test account add response={response.text}")

    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set account_1_id so we can use it later.
    account_2_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def accounts_get_all_test(get_auth_token):
    """
    Account: get all
    - notes
      + should have at the least 2 accounts (pytest and pytest-2)
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/accounts/",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) >= 2


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def accounts_update_test(get_auth_token):
    """
    Account: update
    """

    global account_2
    account_2["_id"] = account_2_id
    account_2["name"] = f"{account_2['name']} Updated"
    account_2["note"] = f"{account_2['note']} Updated"
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/accounts/",
            params=account_2,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def accounts_get_one_test(get_auth_token):
    """
    Account: get a specific profile
    - notes
      + we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global account_2_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/accounts/{account_2_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    # NOTE: we only check the items which can be updated.
    assert response.json()["name"] == account_2["name"]
    assert response.json()["note"] == account_2["note"]
    assert response.json()["enabled"] == account_2["enabled"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def tasks_1_add_test(get_auth_token):
    """
    Task: Add - add a new Task.
    """

    global task_1_id, task_1
    task_1["account"] = account_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/tasks/",
            params=task_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set _id so we can use it later.
    task_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def tasks_1_get_all_test(get_auth_token):
    """
    Account: get all
    - notes
      + should have one Task (task_1)
    """

    _params = {"account": task_1["account"]}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/tasks/",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) == 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def tasks_1_update_test(get_auth_token):
    """
    Task: update - update task_1 Task.
    """

    global task_1
    task_1["_id"] = task_1_id
    task_1["name"] = f"{task_1['name']} Updated"
    task_1["description"] = f"{task_1['description']} Updated"
    task_1["enabled"] = True
    task_1["available_units"] = 50

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/tasks/",
            params=task_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def tasks_1_get_one_test(get_auth_token):
    """
    Task: get a specific Task (task_1).
    - notes
      + we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global task_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/tasks/{task_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    # NOTE: we only check the items which can be updated.
    assert response.json()["name"] == task_1["name"]
    assert response.json()["description"] == task_1["description"]
    assert response.json()["enabled"] == task_1["enabled"]
    assert response.json()["available_units"] == task_1["available_units"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def rewards_1_add_test(get_auth_token):
    """
    Reward: Add - add a new Reward.
    """

    global reward_1_id, task_1
    reward_1["account"] = account_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/rewards/",
            params=reward_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_rewards_1_add response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set _id so we can use it later.
    reward_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def rewards_1_get_all_test(get_auth_token):
    """
    Reward: get all
    - notes
      + should have one Reward (reward_1)
    """

    _params = {"account": task_1["account"]}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/rewards/",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) == 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def rewards_1_update_test(get_auth_token):
    """
    Reward: update - update reward_1 Reward.
    """

    global reward_1
    reward_1["_id"] = reward_1_id
    reward_1["name"] = f"{reward_1['name']} Updated"
    reward_1["description"] = f"{reward_1['description']} Updated"
    reward_1["enabled"] = True
    reward_1["available_units"] = 50

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/rewards/",
            params=reward_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def rewards_1_get_one_test(get_auth_token):
    """
    Reward: get a specific reward
    - notes
      + we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global reward_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/rewards/{reward_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    # NOTE: we only check the items which can be updated.
    assert response.json()["name"] == reward_1["name"]
    assert response.json()["description"] == reward_1["description"]
    assert response.json()["enabled"] == reward_1["enabled"]
    assert response.json()["available_units"] == reward_1["available_units"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_get_total_points_pre_test(get_auth_token):
    """
    Point: get total points before adding any points.
    """

    _params = {
        "account": account_1_id,
        # "username": xx,
    }
    # ~ logger.info(f" test_points_get_total_points_pre params={_params}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/points/totals/",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_get_total_points_pre response: {response.text}")
    assert response.status_code == 200
    assert response.json() == {"created": 0.0, "approved": 0.0, "denied": 0.0}


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_1_add_task_test(get_auth_token):
    """
    Point: Add - add a new Point by Completing a Task (i.e. increase points).
    """

    global point_1_id, point_1
    point_1["account"] = account_1_id
    point_1["point_key"] = task_1_id
    logger.info(f" adding point with params={point_1}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/points/",
            params=point_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set _id so we can use it later.
    point_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_get_all_test(get_auth_token):
    """
    Points: get all
    - notes
      + should have one Point (point_1) worth 1 point.
    """

    _params = {
        "account": point_1["account"],
        # "username": xx,
        # "status": PointStatus.created,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/points/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_get_all (1) response: {response.text}")
    assert response.status_code == 200
    assert response.json()["total"] == 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_get_total_points_after_add_task_test(get_auth_token):
    """
    Point: get total points after initial add.
    """

    _params = {
        "account": account_1_id,
        # "username": xx,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/points/totals/",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_get_total_points_after_add response: {response.text}")
    assert response.status_code == 200
    # ~ assert len(response.json()) == 1
    assert response.json() == {
        "created": 1.5,  # note: 0.5 is added as first time use bonus
        "approved": 0.0,
        "denied": 0.0,
    }


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_1_update_task_test(get_auth_token):
    """
    Point: update - update point_1 Point.
    - we use the 'update' to 'approve' the point.
    """

    global point_1
    point_1["_id"] = point_1_id
    point_1["account"] = point_1["account"]
    # ~ point_1["username"] = ""
    point_1["status"] = PointStatus.approved.value
    point_1["note"] = f"{point_1['note']} Updated"
    point_1["message"] = "This point has been updated by pytest!"
    # ~ point_1["quantity"] = 1

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/points/",
            params=point_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_1_get_one_test(get_auth_token):
    """
    Point: get a specific point (and verify it has been updated).
    - notes
      + we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global point_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/points/one/{point_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_1_get_one response: {response.text}")
    assert response.status_code == 200
    # NOTE: we only check the items which can be updated.
    assert response.json()["account"] == point_1["account"]
    assert response.json()["status"] == PointStatus.approved.value
    assert response.json()["note"] == point_1["note"]
    assert response.json()["message"] == point_1["message"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_get_total_points_after_update_task_test(get_auth_token):
    """
    Point: get total points after update (point approval).
    """

    _params = {
        "account": account_1_id,
        # "username": xx,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/points/totals/",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_get_total_points_after_update response: {response.text}")
    assert response.status_code == 200
    # ~ assert len(response.json()) == 1
    assert response.json() == {"created": 0.0, "approved": 1.0, "denied": 0.0}


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def stash_1_add_with_insufficient_points_test(get_auth_token):
    """
    Stash: Add - add a new Stash with insufficient points.
    - notes
      + user should have 1 point and the request is a stash with 100 points so it should fail.
    """

    global stash_1_id, stash_1
    stash_1["account"] = account_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/stashes/",
            params=stash_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 417
    assert (
        response.json()["detail"]
        == "Insufficient funds available for request.<br/> - Account Balance: 1<br/>- Stashes Balance: 0<br/>- Available for Stash: 1<br/>- Requested Amount: 100.0"
    )


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def stash_2_add_with_sufficient_points_test(get_auth_token):
    """
    Stash: Add - add a new Stash with sufficient points.
    - notes
      + user should have 1 point and the request is a stash with 1 points.
    """

    global stash_2_id, stash_2
    stash_2["account"] = account_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/stashes/",
            params=stash_2,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f" stash_2_add_with_sufficient_points: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set _id so we can use it later.
    stash_2_id = response.json()["id"]
    logger.info(f" stash_2_add_with_sufficient_points - stash_2_id={stash_2_id}")


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def stash_get_all_test(get_auth_token):
    """
    Stash: get all
    - notes
      + should have two Stashes (GENERAL & stash_2)
    """

    _params = {"account": account_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/stashes/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_stash_get_all: {response.text}")
    assert response.status_code == 200
    assert len(response.json()) == 2


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def stash_2_update_test(get_auth_token):
    """
    Stash: update - update stash_2 Stash.
    """

    global stash_2
    stash_2["_id"] = stash_2_id
    # ~ stash_2["account"] = account_1_id
    stash_2["label"] = f"{stash_2['label']} Updated"
    stash_2["note"] = f"{stash_2['note']} Updated by pytest"
    stash_2["edit_type"] = StashEditType.edit.value

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/stashes/",
            params=stash_2,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" stash_2_update: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def stash_2_get_one_test(get_auth_token):
    """
    Stash: get a specific Stash (stash_2).
    - notes
      + we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    # ~ global stash_2_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/stashes/one/{stash_2_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" stash_2_get_one: {response.text}")
    assert response.status_code == 200
    # NOTE: we only check the items which we updated.
    assert response.json()["label"] == f"{stash_2['label']}"
    assert response.json()["note"] == f"{stash_2['note']}"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_2_add_reward_test(get_auth_token):
    """
    Point: Add - add a new Point by redeeming a Reward (i.e. decrease points) which
           will take user's point balance from 1 to 0.
    - notes
      + the user should have 1 point before this action
      + the user should have 0 points after this action
    """

    global point_2_id, point_2
    point_2["account"] = account_1_id
    point_2["point_key"] = reward_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/points/",
            params=point_2,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set _id so we can use it later.
    point_2_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_2_update_reward_test(get_auth_token):
    """
    Point: update - update point_2 Point to be approved.
    - we use the 'update' to 'approve' the point.
    """

    global point_2
    point_2["_id"] = point_2_id
    point_2["account"] = point_2["account"]
    # ~ point_1["username"] = ""
    point_2["status"] = PointStatus.approved.value
    # ~ point_1["note"] = f"{point_1['note']} Updated"
    # ~ point_1["message"] = "This point has been updated by pytest!"
    # ~ point_1["quantity"] = 1

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/points/",
            params=point_2,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_get_total_points_after_add_reward_test(get_auth_token):
    """
    Point: get total points after update (point approval).
    """

    _params = {
        "account": account_1_id,
        # "username": xx,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/points/totals/",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_get_total_points_after_update response: {response.text}")
    assert response.status_code == 200
    # ~ assert len(response.json()) == 1
    assert response.json() == {"created": 0.0, "approved": 0.0, "denied": 0.0}


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_2_add_reward_again_test(get_auth_token):
    """
    Point: Add - attempt to add a Point by redeeming a Reward (i.e. decrease points) which
           will take user's point balance from 0 to -1 which should fail.
    - notes
      + the user should have 0 point before this action
      + the user should have -1 points after this action
    """

    global point_2_id, point_2
    point_2["account"] = account_1_id
    point_2["point_key"] = reward_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/points/",
            params=point_2,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_2_add_reward_again response: {response.text}")
    assert response.status_code == 403
    assert response.json()["detail"] == "Not enough points to redeem reward"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_get_leaders_test(get_auth_token):
    """
    Point: get leaders.
    """

    _params = {"account": account_1_id}
    # ~ logger.info(f" test_points_get_leaders params: {_params}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/points/leaders/",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_get_leaders response: {response.text}")
    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json() == [{"username": "tester", "amount": 0.0}]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_get_accounting_totals_test(get_auth_token):
    """
    Point: get accounting totals.
    """

    _params = {"account": account_1_id}
    # ~ logger.info(f" test_points_get_accounting_totals params: {_params}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/points/accounting_totals/",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test_points_get_accounting_totals response: {response.text}")
    assert response.status_code == 200
    # ~ assert len(response.json()) == 1
    assert response.json() == {"credits": 0, "debits": 0}


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def users_me_test(get_auth_token):
    """
    User Me: get 'me' info for logged in user
    - notes
      + this is as the 'app_user' user
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/me",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["username"] == app_user["username"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def users_get_all_test(get_auth_token):
    """
    User: get all

    @TODO: this should likely be changed to a comprehension to see if drad and tester
           is in the results rather than drad as first response.
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()[0]["username"] == "addalie"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def users_update_test(get_auth_token):
    """
    User: update
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/users/{app_user_for_update_username}",
            params=app_user_for_update,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def users_get_one_test(get_auth_token):
    """
    User: get a specific user
    - notes:
      + we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/{app_user_for_update_username}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    # NOTE: we only check the items which can be updated.
    assert response.json()["display_name"] == app_user_for_update["display_name"]
    assert response.json()["avatar"] == app_user_for_update["avatar"]
    assert response.json()["enabled"] == app_user_for_update["enabled"]
    assert response.json()["note"] == app_user_for_update["note"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_messages_add_test(get_auth_token):
    """
    User Message: add
    NOTICES:
      - the to_user_id (who you send message to) should be the same user who has
        the auth token or your 'get*' calls will not be able to see the message
        unless you authenticate as a different user.
    """

    global user_message_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/user_messages/",
            params=user_message_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test user_message add response={response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # user_message_1_id so we can use it later.
    user_message_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_messages_get_one_test(get_auth_token):
    """
    User Message: get a specific user message
    - notes:
      + we do this *AFTER* the add so we can check that the data has been persisted!
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/user_messages/one/{user_message_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test user_message get one response={response.text}")
    assert response.status_code == 200
    assert response.json()["to_user_id"] == user_message_1["to_user_id"]
    assert response.json()["status"] == "new"
    assert response.json()["category"] == user_message_1["category"]
    assert response.json()["priority"] == user_message_1["priority"]
    assert response.json()["subject"] == user_message_1["subject"]
    assert response.json()["message"] == user_message_1["message"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_messages_get_all_test(get_auth_token):
    """
    User Message: get all
    - notes
      + this is for currently authenticated user so we should have only one (user_message_1)
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/user_messages/all",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f" test user_message get all response={response.text}")
    assert response.status_code == 200
    assert len(response.json()) == 1


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ MISCELLANEOUS ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# NOTE: this comes before cleanup to give more time for above to complete.
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def status_test():
    """
    Status
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_BASE}/status",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["status"] == "OK"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def version_test():
    """
    Version
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_BASE}/version",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json() == app_version


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ CLEANUP ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_message_1_delete_test(get_auth_token):
    """
    User Message: delete
    """

    # ~ global user_message_1_id
    params = {"_id": user_message_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/user_messages/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def stashes_2_delete_test(get_auth_token):
    """
    Stash: delete - delete the stash_2 Stash.
    """

    params = {"_id": stash_2_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/stashes/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_1_delete_test(get_auth_token):
    """
    Point: delete - delete the point_1 Task.
    """

    params = {"_id": point_1_id}
    logger.info(f" test_points_1_delete params={params}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/points/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f" test_points_1_delete response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def points_2_delete_test(get_auth_token):
    """
    Point: delete - delete the point_2 Task.
    """

    params = {"_id": point_2_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/points/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def tasks_1_delete_test(get_auth_token):
    """
    Task: delete - delete the task_1 Task.
    """

    params = {"_id": task_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/tasks/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def rewards_1_delete_test(get_auth_token):
    """
    Reward: delete - delete the reward_1 Reward.
    """

    params = {"_id": reward_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/rewards/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def accounts_delete_test(get_auth_token):
    """
    Account: delete
    """

    global account_2_id
    params = {"_id": account_2_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/accounts/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"
