#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime

import aiocouch
from _common.utils import make_id, fix_id
from config.config import couchdb
from config.models import Cdb
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder

logger = logging.getLogger("default")


async def add_user(user=None):
    """
    Add user
    """

    db = await couchdb[Cdb.USERS.value]

    user.created = datetime.now()
    user.updated = datetime.now()

    try:
        doc = await db.create(make_id(items=[user.username]), data=jsonable_encoder(user))
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{user}'.")


async def determine_username(current_user=None, username: str = None):
    """
    Determine username given supplied username and current users' role.
    """

    from auth.utils import has_role

    if not username:
        return current_user.username
    if await has_role(current_user, "admins"):
        logger.debug("- user is admins")
        return username
    else:
        logger.debug("- user is *not* an admins")
        return current_user.username


async def get_by_username(username):
    """
    Get user by username
    """

    db = await couchdb[Cdb.USERS.value]

    try:
        return await db[username]
    except aiocouch.exception.NotFoundError:
        # NOTICE: we cannot raise exception here as downstream (ldap auth check) needs to account for no user found (so it can create).
        return None


async def get_by_notify_id(notify_id: str = None):
    """
    Get user by notify_id - this is used by the SSE [setup] stream.
    """

    from users.models import User

    db = await couchdb[Cdb.USERS.value]

    selector = {
        "enabled": True,
        "notify_id": notify_id,
    }

    docs = []
    async for doc in db.find(selector=selector):
        docs.append(fix_id(doc))

    if len(docs) == 1:
        return User(**docs[0])
    else:
        logger.error(
            f"Error Getting User By Notify Id: '{notify_id}', lookup found '{len(docs)}' matches. Matches are as follow:\n{docs}"
        )
        return User()


async def update_user(user=None):
    """
    Update user
    """

    logger.debug(f"- updating user with: {user}")
    user.updated = datetime.now()

    try:
        # note: we overlay db user with passed in user obj.
        doc = await get_by_username(user.username)
        doc["enabled"] = user.enabled
        doc["note"] = user.note
        doc["accounts"] = user.accounts
        doc["first_name"] = user.first_name
        doc["last_name"] = user.last_name
        doc["display_name"] = user.display_name
        doc["email"] = user.email
        doc["avatar"] = user.avatar
        doc["user_timezone"] = user.user_timezone
        doc["notify_id"] = user.notify_id

        # note: need to encode accounts and preferences
        doc["accounts"] = jsonable_encoder(user.accounts)
        doc["preferences"] = jsonable_encoder(user.preferences)

        logger.debug(f"- saving user doc as: {doc}")
        await doc.save()

        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{user}'.")


async def verify_user(user=None):
    """
    Verify given user exists.
    Returns the user if found else Raise 404.
    """

    if user:
        return await get_by_username(user)
    else:
        raise HTTPException(status_code=404, detail="Not found")
