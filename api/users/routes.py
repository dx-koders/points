#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from typing import List
from uuid import UUID

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id
from config.config import couchdb
from fastapi import APIRouter, Body, Depends, HTTPException, Query, Security
from fastapi.encoders import jsonable_encoder
from pydantic import HttpUrl
from users.models import (
    User,
    UserAccount,
    UserBase,
    UserPreferences,
    UserListResponse,
    _db,
    get_current_active_user,
    get_current_user,
)

logger = logging.getLogger("default")
users_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@users_router.get("/me", response_model=UserBase)
async def me(current_user: UserBase = Depends(get_current_active_user)):
    """
    Get myself
    """

    return current_user


@users_router.get("/", response_model=List[User])
async def get_all(
    account: str = Query(None, description="account to return users from (leave empty for all)"),
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    db = await couchdb[_db.value]

    docs = []
    selector = {}
    if account:
        selector["accounts"] = {"id_": account}

    logger.debug(f"get_all selector: {selector}")
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        docs.append(User(**fix_id(doc)))

    return docs


@users_router.get(
    "/users_list",
    response_model=List[UserListResponse],
)
async def get_users_list(
    limit: int = 100,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all enabled users, returning the UsersList format of User.
    """

    db = await couchdb[_db.value]
    selector = {
        "enabled": True,
    }
    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        docs.append(UserListResponse(**doc))

    return docs


# NOTICE: we do not need an 'add' route in api as users are added through
#   ldap; however, there is a non-exposed add in utils.py.


@users_router.get("/{username}", response_model=User)
async def get_one(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """Get by username"""

    db = await couchdb[_db.value]

    return User(**fix_id(await _get_or_404(username, db)))


# 2020-07-01:drad update of user is limited to updating the following as other items are set via ldap:
#   - disabled: this will disable the user in the app
#   - note: an app specific note for the user
#   - accounts: a list of accounts the user belongs to
@users_router.put(
    "/{username}",
    response_model=dict,
)
async def update(
    username: str,
    enabled: bool = Query(True, description="Is the user enabled?"),
    display_name: str = Query(None, description="name used in application display"),
    avatar: HttpUrl = Query(None, description="user's avatar"),
    note: str = Query(None, description="note on user"),
    notify_id: str = Query(None, description="user notify id"),
    accounts: List[UserAccount] = None,
    preferences: UserPreferences = Body(None),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update
    """

    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    # @TODO currently dont allow update of account and name for now (need to update pk)
    doc["enabled"] = enabled
    doc["note"] = note if note else doc["note"]
    doc["display_name"] = display_name if display_name else doc["display_name"]
    doc["avatar"] = jsonable_encoder(avatar) if avatar else doc["avatar"]
    doc["preferences"] = jsonable_encoder(preferences) if preferences else doc["preferences"]
    doc["notify_id"] = UUID(notify_id) if notify_id else None
    doc["accounts"] = jsonable_encoder(accounts) if accounts else doc["accounts"]
    logger.debug(f"doc before save: {doc}")
    await doc.save()

    return await doc.info()


@users_router.delete("/{username}", response_model=dict)
async def delete(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    db = await couchdb[_db.value]

    resp = DocInfo(ok=False, id=username, rev="", msg="unknown")
    doc = await _get_or_404(username, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
