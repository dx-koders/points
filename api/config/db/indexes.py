# Application Database Configuration for: indexes

"""
This file contains all explicitly created indexes for the application and is ran
upon application start (via check_databases() on fastapi 'startup' event).

Structure:
  - get current index contents with {db}/_index
  - add an entry in the list below as:
    views.append({"database": "{db}", "views": [{ <contents from get response> }, {etc...}]})
    indexes.append({"database": "activities", "indexes": [{ <contents from get response> }, {etc...}]})
"""

indexes = []

#
# Create ONE instance per database which can have one or more indexes.
#

indexes.append(
    {
        "database": "points",
        "indexes": [
            {
                "ddoc": "_design/index-created",
                "def": {"fields": [{"created": "desc"}]},
                "name": "create",
                "partitioned": False,
                "type": "json",
            },
        ],
    }
)
