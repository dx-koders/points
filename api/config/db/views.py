# Application Database Configuration for: views
"""
This file contains all views for the application and is ran upon application
start (via check_databases() on fastapi 'startup' event).

Structure:
  - get current view contents with `{db}/_design/{design-name}` (e.g. `http $cdb/points/_design/pa`)
  - add an entry in the list below as:
    views.append({"database": "{db}", "views": [{ <contents from get response> }, {etc...}]})
    + NOTE: you do not need to remove the _id, _rev, etc as the _check_db_views method will handle it all.

Tips:
  - one view to a DesignDocument (this keeps it simple and more safe on updates)
  - DesignDocument name should be lower camel case (e.g. activityStream)
  - View name should be dash separated (e.g. activity-stream)

"""

views = []

#
# Create ONE instance per database which can have one or more design docs which
#   can have one or more views.
#

#
# Activity Stream
#
views.append(
    {
        "database": "points",
        "views": [
            {
                "_id": "_design/pa",
                "_rev": "48-f69e40339f8a9f43701c2244371223f0",
                "language": "javascript",
                "views": {
                    "points_credits": {
                        "map": "function (doc) {\n  if ( doc.user && doc.point_type && doc.status == 'approved' && doc.point_type == 'rewards' ) {\n    emit([doc.user], ( doc.quantity * doc.amount ));\n  }\n}",
                        "reduce": "_sum",
                    },
                    "points_debits": {
                        "map": "function (doc) {\n  if ( doc.user && doc.point_type && doc.status == 'approved' && doc.point_type == 'tasks' ) {\n    emit([doc.user], ( doc.quantity * doc.amount ));\n  }\n}",
                        "reduce": "_sum",
                    },
                    "total_approved": {
                        "map": "function (doc) {\n  if ( doc.user && doc.point_type && doc.status == 'approved' ) {\n    emit([doc.account,doc.user], ( doc.quantity * doc.amount));\n  }\n}",
                        "reduce": "_sum",
                    },
                },
            }
        ],
    }
)
