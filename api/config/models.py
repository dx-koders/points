#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

from datetime import date
from enum import Enum

from pydantic import BaseModel


class Cdb(Enum):
    """
    Available Couchdb DBs
    """

    ACCOUNTS = "accounts"
    LOGS = "logs"
    POINTS = "points"
    REWARDS = "rewards"
    SSE = "sse"
    STASHES = "stashes"
    TASKS = "tasks"
    USERS = "users"
    USER_MESSAGES = "user_messages"


class ConfigFastApi(BaseModel):
    """
    FastAPI specific config.
    """

    debug: bool = False


class ConfigApiVersions(BaseModel):
    """
    API Versions.
    """

    current: str = None
    supported: str = None
    deprecated: str = None
    unsupported: str = None


class ConfigApiJwt(BaseModel):
    """
    API JWT config.
    """

    secret_key: str = None
    algorithm: str = None


# ~ class ConfigRouteAuthItem(BaseModel):
# ~ """
# ~ Route Auth Item.
# ~ """

# ~ get_all: Optional[list] = None
# ~ get_one: Optional[list] = None
# ~ add: Optional[list] = None
# ~ update: Optional[list] = None
# ~ delete: Optional[list] = None


# ~ class ConfigRouteAuth(BaseModel):
# ~ """
# ~ Route Auth config.
# ~ """

# ~ accounts: ConfigRouteAuthItem = None
# ~ points: ConfigRouteAuthItem = None
# ~ rewards: ConfigRouteAuthItem = None
# ~ tasks: ConfigRouteAuthItem = None
# ~ users: ConfigRouteAuthItem = None


class ConfigApi(BaseModel):
    """
    API specific config.
    """

    versions: ConfigApiVersions = None
    jwt: ConfigApiJwt = None
    # NOTE: route_auth is not parsed as it is used by inspect.frame which is easier as a raw dict.
    route_auth: dict = None


class ConfigCore(BaseModel):
    """
    Core specific config.
    """

    name: str = None
    description: str = None
    version: str = None
    created: date = None
    modified: date = None


class ConfigBase(BaseModel):
    """
    Config Base.
    """

    fastapi: ConfigFastApi = None
    api: ConfigApi = None
    core: ConfigCore = None
    # database:
