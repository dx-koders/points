#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import inspect
import logging
from typing import List

from _common.utils import fix_id, get_route_roles
from accounts.models import Account
from accounts.routes import get_account
from auth.models import Role
from auth.utils import has_account_access
from config.config import DB
from fastapi import APIRouter, HTTPException, Security, status
from pymongo.errors import DuplicateKeyError
from users.models import UserBase, get_current_user

from .models import Notifier, NotifierBase, NotifierConfig

logger = logging.getLogger("default")
notifiers_router = APIRouter()


async def _get_or_404(
    name: str,
    account: Account,
    current_user: UserBase,
    role: Role,
):
    # check to ensure use has access for account.
    if not await has_account_access(current_user, account, roles=role):
        raise HTTPException(status_code=401, detail="Not authorized")

    obj = await DB.notifier.find_one({"name": name, "account": account.id_})
    if obj:
        # replace account (id) with account name
        obj["account"] = account.name
        return fix_id(obj)
    else:
        raise HTTPException(status_code=404, detail="Not found")


@notifiers_router.get("/", response_model=List[Notifier])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """Get all"""

    user_accounts = [a.id_ for a in current_user.accounts]

    model_cursor = (
        # ~ DB.notifier.find({"account": {"$in": user_accounts}}).skip(skip).limit(limit)
        # note: the following aggregate performs a join to replace the account (id) with the account name.
        DB.notifier.aggregate(
            [
                {"$match": {"account": {"$in": user_accounts}}},
                {
                    "$lookup": {
                        "let": {"account_obj": {"$toObjectId": "$account"}},
                        "from": "account",
                        "pipeline": [
                            {"$match": {"$expr": {"$eq": ["$_id", "$$account_obj"]}}},
                            {"$project": {"name": 1, "_id": 0}},
                        ],
                        "as": "account",
                    }
                },
                {"$unwind": "$account"},
                {
                    "$project": {
                        "_id": 1,
                        "name": 1,
                        "account": "$account.name",
                        "kind": 1,
                        "config": 1,
                        "note": 1,
                        "disabled": 1,
                    }
                },
            ]
        )
    )
    r = await model_cursor.to_list(length=limit)
    return list(map(fix_id, r))


@notifiers_router.get("/{name}", response_model=Notifier)
async def get_by_name(
    name: str,
    account: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """Get by name/account"""

    acc = await get_account(account)
    return await _get_or_404(name, acc, current_user, role=get_route_roles(inspect.currentframe()))


@notifiers_router.post("/", response_model=Notifier, status_code=status.HTTP_201_CREATED)
async def add(
    obj: NotifierBase,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """Add new"""

    # check to ensure use has access for account.
    acc = await get_account(obj.account)
    if not await has_account_access(current_user, acc, roles=get_route_roles(inspect.currentframe())):
        raise HTTPException(status_code=401, detail="Not authorized")

    o = NotifierBase()
    o.name = obj.name
    o.account = acc.id_
    o.kind = obj.kind
    o.config = obj.config
    o.note = obj.note
    o.disabled = obj.disabled

    try:
        r = await DB.notifier.insert_one(o.dict())
        if r.inserted_id:
            return await _get_or_404(o.name, acc, current_user, role=get_route_roles(inspect.currentframe()))
    except DuplicateKeyError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{obj.name}'.")


@notifiers_router.put(
    "/{name}",
    response_model=Notifier,
)
async def update(
    name: str,
    account: str,
    config: NotifierConfig = None,
    disabled: bool = False,
    note: str = None,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """Update by name/account"""
    # NOTICE: users cannot update account or kind (need to delete and re-add)

    # check to ensure use has access for account.
    acc = await get_account(account)
    if not await has_account_access(current_user, acc, roles=get_route_roles(inspect.currentframe())):
        raise HTTPException(status_code=401, detail="Not authorized")

    obj_data = {"config": config.dict(), "disabled": disabled, "note": note}
    obj_op = await DB.notifier.update_one({"name": name, "account": acc.id_}, {"$set": obj_data})
    if obj_op.modified_count:
        return await _get_or_404(name, acc, current_user, role=get_route_roles(inspect.currentframe()))
    else:
        raise HTTPException(status_code=304)


@notifiers_router.delete("/{name}", response_model=dict)
async def delete_by_name(
    name: str,
    account: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """Delete by name"""

    # check to ensure use has access for account.
    acc = await get_account(account)
    if not await has_account_access(current_user, acc, roles=get_route_roles(inspect.currentframe())):
        raise HTTPException(status_code=401, detail="Not authorized")

    obj_op = await DB.notifier.delete_one({"name": name, "account": acc.id_})
    if obj_op.deleted_count:
        return {"status": f"deleted count: {obj_op.deleted_count}"}
