#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

from datetime import datetime
from enum import Enum
from typing import Optional

from pydantic import BaseModel


class NotifierKind(str, Enum):
    email = "email"
    rocketchat = "rocketchat"
    # slack = "slack"
    # sms = "sms"
    # pagerduty = "pagerduty"
    # webhook = "webhook"
    # wechat = "wechat"


class NotifierConfig(BaseModel):
    """
    Notification config
    """

    server: str = None
    username: str = None
    password: str = None
    msg_from: str = None
    msg_to: Optional[str] = None  # not utilized for email
    msg_subject: str = None
    msg_prefix: str = None
    msg_suffix: str = None


class NotifierBase(BaseModel):
    """
    Base
    """

    name: str = None
    account: str = None  # id of the account to which the notifier belongs
    kind: NotifierKind = None
    config: Optional[NotifierConfig] = None

    note: Optional[str] = None
    disabled: bool = False


class NotifierExt(NotifierBase):
    """
    Extended (added by backend logic)
    """

    created: datetime = datetime.now()


class Notifier(NotifierExt):
    """
    Actual (at DB level)
    """

    id_: str


class NotifierMessage(BaseModel):
    """
    Notification message
    a generic notifier message structure used in sending notifications
    """

    notifier: Notifier = None  # used to store the notifier
    destination: str = None  # to, recipient, etc.
    subject: str = None  # message subject
    body: str = None  # message body
