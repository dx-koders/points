#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
from datetime import datetime
from typing import Optional

import aiocouch
from _common.utils import make_id
from _common.models import DocInfo
from config.config import couchdb
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from user_messages.models import (
    UserMessageCategory,
    UserMessageExt,
    UserMessagePriority,
    UserMessageStatus,
    _db,
)
from sse.models import SSEDataEventType, SSEDataActions
from sse.utils import add_message

logger = logging.getLogger("default")


async def add_user_message(
    to_user_id: str = None,
    to_user_account: str = None,
    from_user_id: Optional[str] = None,
    category: UserMessageCategory = None,
    priority: UserMessagePriority = None,
    subject: str = None,
    message: str = None,
    creator: str = None,
):
    """
    Add a message.
    """

    from users.utils import get_by_username

    db = await couchdb[_db.value]

    # check if 'to' user exists.
    _to = await get_by_username(to_user_id)
    if not _to:
        raise HTTPException(status_code=424, detail="To user does not exist.")
    # if 'from' user present, make sure the user exists.
    if from_user_id:
        _from = await get_by_username(from_user_id)
        if not _from:
            raise HTTPException(status_code=424, detail="From user does not exist.")

    _user_message = UserMessageExt(
        to_user_id=to_user_id,
        from_user_id=from_user_id,
        status=UserMessageStatus.new,
        category=category,
        priority=priority,
        subject=subject,
        message=message,
        created=datetime.now(),
        updated=datetime.now(),
        creator=creator,
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_user_message),
        )
        await doc.save()

        _ret: DocInfo = await add_message(
            user=to_user_id,
            account=to_user_account,
            event_type=SSEDataEventType.user_message.value,
            data=SSEDataActions.new.value,
            created_by="user_messages.utils.add_user_message",
        )
        if not _ret or "ok" not in _ret or not _ret["ok"]:
            logger.error(
                f"Error sending SSE message to user [{to_user_id}/{to_user_account}], event_type={SSEDataEventType.user_message.value}, data={SSEDataActions.new.value}"
            )

        return await doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None
