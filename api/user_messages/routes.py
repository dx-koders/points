#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id
from config.config import couchdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from user_messages.models import (
    UserMessageCategory,
    UserMessagePriority,
    UserMessageResponse,
    _db,
)
from user_messages.utils import add_user_message
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
user_messages_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@user_messages_router.get("/all", response_model=List[UserMessageResponse])
async def get_all(
    limit: int = Query(500, ge=1, le=9999, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all user messages for current authenticated user.
    """

    # logger.debug(f"- get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    selector = {"to_user_id": current_user.username}

    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        docs.append(UserMessageResponse(**fix_id(doc)))

    return docs


@user_messages_router.get("/one/{_id}", response_model=UserMessageResponse)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    db = await couchdb[_db.value]

    return UserMessageResponse(**fix_id(await _get_or_404(_id, db)))


@user_messages_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    # all new are added with 'new' status
    # status: MessageStatus = Query(..., description="the status of the message"),
    to_user_id: str = Query(..., description="user the message is being sent to"),
    # ~ from_user_id: str = Query(
    # ~ None, description="user sending the message (if applicable)"
    # ~ ),
    to_user_account: str = Query(..., description="user account the message is being sent to"),
    category: UserMessageCategory = Query(UserMessageCategory.user, description="the category of the message"),
    priority: UserMessagePriority = Query(UserMessagePriority.normal, description="the priority of the message"),
    subject: str = Query(..., description="subject of the message"),
    message: str = Query(..., description="the message"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new User Message
    NOTICES:
    - we are not overly concerned with ensuring 'from_user' is enabled as they must be enabled to log in
    - from user is always currently authenticated user (note: system messages should use the utils.add_user_message to get around this)
    """

    # check to ensure to_user_id exists and is enabled.
    from users.utils import verify_user

    _to_user = await verify_user(user=to_user_id)
    if _to_user and _to_user["enabled"]:
        return await add_user_message(
            to_user_id=to_user_id,
            to_user_account=to_user_account,
            from_user_id=current_user.username,
            category=category,
            priority=priority,
            subject=subject,
            message=message,
            creator=current_user.username,
        )
    else:
        # pass
        # return a failure
        logger.error(
            f"403 Forbidden Error occurred as a result of user ({current_user.username}) trying to send a message to user ({to_user_id}); however {to_user_id} does not exist or is not enabled (enabled={_to_user['enabled']})"
        )
        raise HTTPException(status_code=403, detail="Forbidden")


@user_messages_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str = Query(..., description="the id of the user message to delete"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Hard Delete by id
    """

    db = await couchdb[_db.value]

    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
