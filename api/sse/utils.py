#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
import aiocouch
from typing import List
from sse.models import SSEDataEventType, SSEDataExt, SSEData, SSEDataStatus, SSEDataCountResponse
from fastapi import HTTPException
from config.config import couchdb
from config.models import Cdb
from _common.models import DocInfo
from _common.utils import make_id, fix_id
from fastapi.encoders import jsonable_encoder
from datetime import datetime

logger = logging.getLogger("default")


async def add_message(
    user: str = None,
    account: str = None,
    event_type: SSEDataEventType = None,
    data: str = None,
    created_by: str = None,
) -> DocInfo:
    """
    Add a Messages: this adds a message for a given user.
    NOTES:
      - this action is performed by code and is intended to 'notify' the client (browser) of new data
    """

    logger.debug(f"add_ssedata with event_type={event_type}")
    db = await couchdb[Cdb.SSE.value]

    _sse: SSEDataExt = SSEDataExt()
    _sse.user = user
    _sse.account = account
    _sse.status = SSEDataStatus.new
    _sse.event_type = event_type
    _sse.data = data
    _sse.created_by = created_by
    _sse.created = datetime.now()
    _sse.updated = datetime.now()

    try:
        doc = await db.create(make_id(items=[account, user, str(uuid.uuid4())]), data=jsonable_encoder(_sse))
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{user}'.")


async def message_count(
    user: str = None,
    account: str = None,
) -> SSEDataCountResponse:
    """
    Get Count Of SSEData for account:user.
    NOTES:
    - only records with status='NEW' are subject to count.
    @TODO:
    - we are using a simple query now but this should likely use a view
      for performance reasons.
    """

    db = await couchdb[Cdb.SSE.value]

    selector = {
        "_id": {"$regex": f"^{account}:{user}:.*$"},
        "status": "NEW",
    }
    # ~ logger.debug(f"message count selector: {selector}")
    cnt = 0
    async for doc in db.find(selector=selector):
        cnt += 1

    return cnt


async def get_messages(
    user: str = None,
    account: str = None,
) -> List[SSEData]:
    """
    Retrieve Messages: this gets all Message(s) for an account/user that have a status of NEW.
    NOTES:
    - only records with status='NEW' are subject to count.
    """

    db = await couchdb[Cdb.SSE.value]

    selector = {
        "_id": {"$regex": f"^{account}:{user}:.*$"},
        "status": "NEW",
    }
    # ~ logger.debug(f"get_message selector: {selector}")
    docs = []
    # note: we get the first message only
    async for doc in db.find(selector=selector):
        docs.append(SSEData(**fix_id(doc)))

    # logger.debug(f"- returning points of: {docs}")
    return docs


async def mark_message(
    id_: str = None,
    status: SSEDataStatus = None,
) -> DocInfo:
    """
    Update Message: this allows updating of a SSEData message to set it as delivered.
    """

    # ~ logger.debug(f"Marking message with id={id_} to status={status.value}")
    db = await couchdb[Cdb.SSE.value]
    doc = await db[id_]
    # ~ logger.debug(f"doc is: {doc}")
    doc["status"] = status.value
    doc["updated"] = jsonable_encoder(datetime.now())
    await doc.save()
    return await doc.info()


def split_channel(channel: str = None):
    """
    Split a channel into its {account}:{user} values.
    """

    return channel.split(":")
