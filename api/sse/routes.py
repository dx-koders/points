#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from starlette.requests import Request
from sse_starlette.sse import EventSourceResponse
import asyncio
from typing import List
from fastapi import status, APIRouter, HTTPException, Query, Security
from users.models import UserBase, get_current_user
from _common.models import DocInfo
from users.utils import get_by_notify_id
from sse.models import SSEData, SSEDataEventType, SSEDataCountResponse, ESMessage, SSEDataStatus
from sse.utils import add_message, get_messages, mark_message, message_count, split_channel

logger = logging.getLogger("default")
STREAM_DELAY = 1  # second
RETRY_TIMEOUT = 15000  # millisecond

sse_router = APIRouter()


# channel is in format '{notify_id}:{account}'
@sse_router.get("/stream/{channel}")
async def message_stream(request: Request, channel: str = "default:user"):
    """
    Create the message stream.
    NOTES:
      - the 'channel' supplied is in the format of '{user.notify_id}:{user.account}'.
    """

    logger.info(f"Starting stream for channel={channel} (notify_id:account)")
    notify_id, account = split_channel(channel=channel)
    _user = await get_by_notify_id(notify_id=notify_id)
    if not _user:
        # need to raise and error here as we didn't find a user matching the notify_id.
        raise HTTPException(status_code=403, detail="No User (notify_id) found for channel.")

    user = _user.username
    logger.debug(f"User found: username={user}, account={account} (user.accounts={_user.accounts})")

    # channel needs to be of the form: {account}:{user}
    async def new_messages(account=None, user=None):
        # ~ logger.debug(f"Checking for new message with account={account}, user={user}")
        if await message_count(user=user, account=account) > 0:
            logger.debug(f"FOUND MESSAGES for account={account}, user={user}")
            return True
        else:
            # ~ logger.debug("User channel does not have messages.")
            return None

    async def event_generator(account=None, user=None):
        while True:
            # If client closes connection, stop sending events
            if await request.is_disconnected():
                break

            # Checks for new messages and return them to client if any
            if await new_messages(account=account, user=user):
                # get any/all messages.
                logger.debug(f"get_messages for user={user} and account={account}")
                messages = await get_messages(
                    user=user,
                    account=account,
                )
                for message in messages:
                    # mark_message_sent so it is not picked up/sent again
                    _ret = await mark_message(
                        id_=message.id_,
                        status=SSEDataStatus.sent,
                    )
                    if not _ret or not _ret["ok"]:
                        logger.error(f"Error Marking Message, result was: {_ret}")
                    # note: we need to cast to dict (model_dump()) or the yielded message is not properly formatted.
                    _esm = ESMessage(
                        id=channel, event=message.event_type.value, retry=RETRY_TIMEOUT, data=message.data
                    ).model_dump()
                    logger.debug(f">> SSE-Stream [{user}:{account}] sending response of: {_esm}")
                    yield _esm

            await asyncio.sleep(STREAM_DELAY)

    return EventSourceResponse(event_generator(account=account, user=user), media_type="text/event-stream")


@sse_router.post("/manual_add", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    user: str = Query(..., description="user to add sse message for"),
    account: str = Query(..., description="account of user to add sse message for"),
    event_type: SSEDataEventType = Query(..., description="type of event"),
    data: str = Query(..., description="data to be sent"),
    created_by: str = Query(..., description="who/what created the message"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manual add of SSEData message.
    """

    logger.debug(f"Received manual add with event_type={event_type}")
    _ret: DocInfo = await add_message(
        user=user,
        account=account,
        event_type=event_type,
        data=data,
        created_by=created_by,
    )

    return _ret


@sse_router.post("/manual_message_count", response_model=SSEDataCountResponse, status_code=status.HTTP_201_CREATED)
async def manual_message_count(
    user: str = Query(..., description="user to add sse message for"),
    account: str = Query(..., description="account of user to add sse message for"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manual get Message count.
    """

    logger.debug(f"Received manual get_data_count with user={user} and account={account}...")
    _ret = await message_count(
        user=user,
        account=account,
    )

    _count = SSEDataCountResponse()
    _count.count = _ret

    return _count


@sse_router.post("/manual_get_messages", response_model=List[SSEData], status_code=status.HTTP_201_CREATED)
async def manual_get_messages(
    user: str = Query(..., description="user to add sse message for"),
    account: str = Query(..., description="account of user to add sse message for"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manual get Messages.
    """

    logger.debug(f"Received manual get_messages with user={user} and account={account}...")
    _ret = await get_messages(
        user=user,
        account=account,
    )

    return _ret
