#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

# SSE: Server Sent Event logic contains two key components for the application:
# 1. stream: the SSE stream that clients (browser) connect to in order to receive server sent events
#    - NOTE: the stream is handled by a route in the route module
# 2. data structure: the data structure that stores 'messages' that need to be sent to a channel
#    - NOTE: the model, most of the route, and utils are for the data structure


from datetime import datetime
from enum import Enum

from config.models import Cdb
from pydantic import BaseModel

_db = Cdb.SSE


class SSEDataStatus(str, Enum):
    new = "NEW"
    sent = "SENT"


class SSEDataEventType(str, Enum):
    user_message = "user_message"
    task = "task"
    reward = "reward"
    point = "point"


class SSEDataActions(str, Enum):
    new = "new"
    add = "add"
    update = "update"
    delete = "delete"


class SSEDataBase(BaseModel):
    """
    Base
    """

    status: SSEDataStatus = None
    user: str = None
    account: str = None
    event_type: SSEDataEventType = None  # should be typed
    data: str = None
    created_by: str = None  # who (or what) created the SSEData.


class SSEDataExt(SSEDataBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiating as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()


class SSEData(SSEDataExt):
    """
    Actual (at DB level)
    """

    id_: str


class SSEDataCountResponse(BaseModel):
    """
    SSEData Count Response.
    """

    count: int = None


class ESMessage(BaseModel):
    """
    EventSource Message.
    """

    id: str = None
    event: str = None
    retry: int = 0
    data: str = None
