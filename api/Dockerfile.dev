# https://hub.docker.com/_/python
FROM python:3.13-alpine3.21
MAINTAINER drad <sa@adercon.com>

ARG DEPLOY_ENV

# note: need to copy requirements to root as /app is not mounted in .dev builds.
COPY requirements.txt requirements.dev.txt /
WORKDIR /app

# the following are needed to build psycopg2
RUN echo "NOTICE: Building for DEPLOY_ENV=${DEPLOY_ENV}"                       \
  # temp: perform apk upgrade for temp CVE fix                                 \
  && apk upgrade                                                               \
  && apk add --no-cache                                                        \
    python3-dev                                                                \
  && apk add --no-cache --virtual .build_deps                                  \
    tzdata                                                                     \
    gcc                                                                        \
    musl-dev                                                                   \
    # the following needed for cryptography                                    \
    libffi-dev                                                                 \
  # set timezone                                                               \
  && cp "/usr/share/zoneinfo/America/New_York" /etc/localtime                  \
  #                                                                            \
 && if [ "${DEPLOY_ENV}" == "dev" ]; then                                      \
      echo "install dev reqs..."                                               \
      && python -m pip install -r /requirements.dev.txt;                       \
    else                                                                       \
      echo "install normal reqs..."                                            \
      && python -m pip install -r /requirements.txt;                           \
    fi                                                                         \
  && apk del .build_deps                                                       \
  && echo "Complete."

EXPOSE 8000

# CMD gunicorn -k uvicorn.workers.UvicornH11Worker -w ${UVICORN_WORKERS} -b :8000 ${UVICORN_OPTIONS} main:app
# CMD uvicorn --workers 1 --log-level "info" --port "8000" --host "0.0.0.0" main:app
