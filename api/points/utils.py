#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging

import aiohttp
import rewards.routes as reward_routes
import tasks.routes as task_routes
from auth.utils import has_role
from config.config import (
    API_NEW_TASK_BONUS_AMOUNT,
    CDB_PASS,
    CDB_URI,
    CDB_USER,
    couchdb,
)
from fastapi import HTTPException
from users.models import UserBase
from time import perf_counter

from points.models import _db, PointType
from tasks.utils import task_usage_adjustment
from rewards.utils import reward_usage_adjustment

logger = logging.getLogger("default")


async def get_account_totals(
    account: str = None,
    username: str = None,
):
    """
    Get points account totals
    - NOTICE: this method uses aiohttp directly as aiocouch does not currently support design docs/views.
    """
    # @TODO: the total_approved 'view' is hard-coded to 'default' account - need to fix this!

    logger.debug(f"- get accounting totals for account={account} and username={username}")
    _response = {
        "credits": 0,
        "debits": 0,
    }

    async with aiohttp.ClientSession() as session:
        params = {
            "reduce": "true",
            "inclusive_end": "true",
            "group": "true",
            # "group_level": "exact",
            "startkey": [username],
            "endkey": [username],
        }

        # get user's debits
        async with session.post(
            f"{CDB_URI}/points/_design/pa/_view/points_debits",
            auth=aiohttp.BasicAuth(CDB_USER, CDB_PASS),
            json=params,
        ) as response:
            j = await response.json()
            logger.debug(f"points_debits view response: j={j}, length={len(j['rows'])}")
            if "rows" in j and len(j["rows"]) > 0:
                logger.debug(f"- response is: {j} --> {j['rows'][0]['value']}")
                _response["debits"] = j["rows"][0]["value"]

        # get user's credits
        async with session.post(
            f"{CDB_URI}/points/_design/pa/_view/points_credits",
            auth=aiohttp.BasicAuth(CDB_USER, CDB_PASS),
            json=params,
        ) as response:
            j = await response.json()
            logger.debug(f"points_credits view response: j={j}, length={len(j['rows'])}")
            if "rows" in j and len(j["rows"]) > 0:
                logger.debug(f"- response is: {j} --> {j['rows'][0]['value']}")
                _response["credits"] = abs(j["rows"][0]["value"])

    return _response


async def get_amount(
    point_type: PointType = None,
    _id: str = None,
    current_user: UserBase = None,
    account: str = None,
    username: str = None,
):
    """
    Get point amount given its type:key (_id).
    - note: this increments task.usage_count if applicable
    - note: this decreases the task.available_units if applicable
    - note: this decreases the reward.available_units if applicable
    """

    from points.routes import get_total_points

    # for point_type:
    #  - task:, task must exist and be enabled
    #  - reward: reward must exist, be enabled, and user must have enough points to redeem the reward

    if point_type == PointType.tasks:
        task = await task_routes.get_one(_id=_id, current_user=current_user)
        # logger.debug(f"- source (task) lookup result: {task}")
        if not task:
            raise HTTPException(status_code=404, detail="Task not found")
        value = task.value
        flag = ""
        if not task.usage_count or task.usage_count == 0:
            # all 'new' tasks get a bonus
            value = value + API_NEW_TASK_BONUS_AMOUNT
            task.usage_count = 1
            flag = f"NOTE: You received a {API_NEW_TASK_BONUS_AMOUNT} bonus as this was the first time this task was submitted!"
        else:
            task.usage_count += 1

        if task.available_units:
            task.available_units -= 1

        await task_usage_adjustment(
            _id=task.id_,
            usage_count=task.usage_count,
            available_units=task.available_units,
        )
        return (
            value,
            flag,
        )
    elif point_type == PointType.rewards:
        reward = await reward_routes.get_one(_id=_id, current_user=current_user)
        # ~ logger.debug(f"- source (reward) lookup result: {reward}")
        if not reward:
            raise HTTPException(status_code=404, detail="Reward not found")
        amount = reward.value
        # get user's current points balance.
        total_points = await get_total_points(account=account, username=username, current_user=current_user)
        if amount > total_points.approved:
            raise HTTPException(status_code=403, detail="Not enough points to redeem reward")

        if reward.available_units:
            reward.available_units -= 1

        await reward_usage_adjustment(_id=reward.id_, available_units=reward.available_units)

        # change amount to negative as we are using a point as opposed to adding it.
        return -amount, None


async def get_points_count(selector: dict = {}):
    """
    Get count of points for given selector
    """

    # @TODO: this should be moved to a view to enhance performance.
    logger.debug(f"Get points count for selector: {selector}")
    start = perf_counter()
    # ~ couchdb = get_cdb()
    db = await couchdb[_db.value]
    cnt = 0
    async for doc in db.find(selector=selector):
        cnt += 1

    end = perf_counter()
    logger.debug(f"Total docs for filter: {cnt} (took {end - start}s)")
    return cnt


async def get_status(current_user: UserBase = None, old_status: str = None, new_status: str = None):
    """
    Get status
    """

    if await has_role(current_user, "admins"):
        logger.debug("- user is admins")
        return new_status
    else:
        logger.debug("- user is *not* an admins")
        return old_status


#
async def notify_via_rocketchat(message: str = ""):
    """
    Notifiy via rocketchat
    @TODO: replace this logic with mattermost
    """

    logger.warning("WARN: rocketchat API has been deprecated")
    return False

    # ~ logger.debug(f"- notifying via rocketchat, server: {API_RC_SERVER_URL}")
    # ~ with sessions.Session() as session:
    # ~ rocket = RocketChat(
    # ~ user_id=API_RC_USER_ID,
    # ~ auth_token=API_RC_TOKEN,
    # ~ server_url=API_RC_SERVER_URL,
    # ~ session=session,
    # ~ )
    # ~ rocket.chat_post_message(
    # f"*New Upload*\n file: {file_id}\n• size: {file_size}\n• expiry: {expiry}",
    # ~ message,
    # ~ channel=API_RC_CHANNEL,
    # ~ alias=cfg.core.name,
    # ~ ).json()

    # ~ return True
