#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import inspect
import logging
import uuid
from datetime import datetime
from typing import List
from uuid import UUID

import aiocouch
import aiohttp
import arrow
from _common.models import DocInfo
from _common.utils import fix_id, get_route_roles, make_id
from accounts.utils import check_access_get_db
from auth.utils import has_role
from config.config import CDB_PASS, CDB_URI, CDB_USER, DEPLOY_ENV
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from user_messages.models import UserMessageCategory, UserMessagePriority
from user_messages.utils import add_user_message
from users.models import UserBase, get_current_user
from users.utils import determine_username
from sse.models import SSEDataEventType, SSEDataActions
from sse.utils import add_message

from points.models import (
    Point,
    PointExt,
    PointsLeaders,
    PointsResponse,
    PointStatus,
    PointsTotals,
    PointType,
    _db,
)
from points.utils import (
    get_account_totals,
    get_amount,
    get_points_count,
    get_status,
    notify_via_rocketchat,
)

logger = logging.getLogger("default")
points_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@points_router.get("/all", response_model=PointsResponse)
async def get_all(
    account: str = Query(..., description="The account of the point"),
    filter_status: PointStatus = Query(None, description="Filter on Status"),
    filter_user: str = Query(None, description="Filter on User"),
    filter_point_type: PointType = Query(None, description="Filter on Point Type"),
    filter_point_key: str = Query(None, description="Filter on Point Key"),
    filter_misc: str = Query(None, description="Filter on miscellaneous items such as Note and Tag"),
    limit: int = 20,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all for account/user
    - filter_user is only applicable for users who are admin
    """

    logger.debug(f"Points get_all with filter_status: {str(filter_status)}")
    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    _response = PointsResponse()

    _filter_user = current_user.username
    if await has_role(current_user, "admins"):
        if filter_user:
            _filter_user = filter_user
        else:
            _filter_user = ""

    selector = {
        "_id": {"$regex": f"^{account}:{_filter_user}.*$"},
    }
    if filter_status:
        selector["status"] = filter_status

    if filter_point_type:
        selector["point_type"] = filter_point_type

    if filter_point_key:
        selector.update({"point_key": {"$regex": f"{filter_point_key}"}})

    if filter_misc:
        selector.update(
            {
                "$or": [
                    {"note": {"$regex": f"{filter_misc}"}},
                    {"tags": {"$elemMatch": {"$eq": filter_misc}}},
                ]
            }
        )

    sort = [{"created": "desc"}]

    logger.debug(f"Points get_all selector: {selector}")
    docs = []
    # NOTICE: db.find uses a sort which requires the appropriate db index(es)
    async for doc in db.find(selector=selector, sort=sort, limit=limit, skip=skip):
        docs.append(Point(**fix_id(doc)))

    _response.total = await get_points_count(selector=selector)
    _response.points = docs
    logger.debug(f"Returning points of: {_response}")
    return _response


@points_router.get("/one/{_id}", response_model=Point)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    return Point(**fix_id(await _get_or_404(_id, db)))


@points_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    account: str = Query(..., description="The account of the point"),
    username: str = Query(None, description="The username of the point (only applicable for admin users)"),
    # amount: float = Query(..., description="The amount of the point"),
    status: PointStatus = Query(PointStatus.created, description="The status of the point"),
    point_type: PointType = Query(..., description="The type of the point"),
    point_key: str = Query(..., description="The key of the Task/Reward to which the Point belongs"),
    note: str = Query(None, description="A note of the point"),
    message: str = Query(None, description="Message about point (e.g. to explain why approved/rejected)"),
    quantity: float = Query(1, description="The quantity for the point (typically just '1')"),
    tid: UUID = Query(
        None,
        description="a uuid used to relate two or more transactions (i.e. send points service)",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    - note: if current user is not admin the username and status are ignored
    """

    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    _username = await determine_username(current_user=current_user, username=username)
    logger.debug("- using username of: {_username}")

    _amount, _message = await get_amount(
        point_type=point_type,
        _id=point_key,
        current_user=current_user,
        account=account,
        username=_username,
    )
    if message:
        if _message:
            _message = f"{_message}\r\n{message}"
        else:
            _message = message
    logger.debug(f"- message={message} and _message={_message}")
    point = PointExt(
        account=account,
        user=_username,
        status=await get_status(current_user=current_user, old_status=PointStatus.created, new_status=status),
        amount=_amount,
        message=_message,
        point_type=point_type,
        point_key=point_key,
        note=note,
        quantity=quantity,
        tid=tid,
        created=datetime.now(),
        updated=datetime.now(),
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[account, _username, str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(point),
        )
        await doc.save()
        r = await doc.info()
        _ret: DocInfo = await add_message(
            user=current_user.username,
            account=account,
            event_type=SSEDataEventType.point.value,
            data=SSEDataActions.add.value,
            created_by="points.routes.add",
        )
        if not _ret or "ok" not in _ret or not _ret["ok"]:
            logger.error(
                f"Error sending SSE message to user [{current_user.username}/{account}], event_type={SSEDataEventType.point.value}, data={SSEDataActions.add.value}"
            )

        # if we were successful in adding the point then send notification.
        # @TODO: replace rc with mm
        API_RC_NOTIFY_ENABLED = False
        if r["ok"] and API_RC_NOTIFY_ENABLED:
            deploy_env = ""
            # show environment flag if environment is not prod
            if DEPLOY_ENV != "prd":
                deploy_env = " - `" + DEPLOY_ENV.upper() + "`"
            await notify_via_rocketchat(
                f"""
**New Point Entry from: {current_user.username}**{deploy_env}
- Type:    {point.point_type} ({point.point_key})
- Points:  {point.amount * point.quantity} ({point.amount} x {point.quantity})
- Note:    {point.note}
- Id:      {doc_id}
- Created: {arrow.get(point.created).to("US/Eastern").format("YYYY-MM-DD HH:mm")}
"""
            )

        return r

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{account}/{username}'.")


@points_router.put("/", response_model=DocInfo)
async def update(
    _id: str,
    account: str = Query(..., description="The account of the point"),
    username: str = Query(None, description="The username of the point (only applicable for admin users)"),
    # amount: float = Query(None, description="The amount of the point"),
    status: PointStatus = Query(None, description="The status of the point"),
    point_type: PointType = Query(None, description="The type of the point"),
    point_key: str = Query(None, description="The key of the point"),
    note: str = Query(None, description="A note of the point"),
    message: str = Query(None, description="Message about point (e.g. to explain why approved/rejected)"),
    quantity: float = Query(None, description="The quantity for the point"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update
    - note: if current user is not admin the status is ignored
    - note: if current user is not admin the message is ignored
    """
    # @TODO currently dont allow update of account and username for now (need to update pk)

    _account = _id.split(":")[0]
    db = await check_access_get_db(
        account=_account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    doc = await _get_or_404(_id, db)
    logger.debug(f"doc is: {doc}")

    if status:
        doc["status"] = await get_status(current_user=current_user, old_status=PointStatus.created, new_status=status)

    if message and await has_role(current_user, "admins"):
        doc["message"] = message

    doc["note"] = note if note else doc["note"]
    # note: amount is only recalculated if point_key is supplied.
    if point_type and point_key:
        doc["point_type"] = point_type
        doc["point_key"] = point_key
        doc["amount"], amount_message = await get_amount(
            point_type=point_type,
            _id=point_key,
            current_user=current_user,
            account=account,
            username=username,
        )

    doc["quantity"] = quantity if quantity else doc["quantity"]
    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    # ~ logger.debug(f"doc before save: {doc}")
    await doc.save()

    # notify current user (this will be the admin who updates the doc).
    _ret: DocInfo = await add_message(
        user=current_user.username,
        account=_account,
        event_type=SSEDataEventType.point.value,
        data=SSEDataActions.update.value,
        created_by="points.routes.update",
    )
    if not _ret or "ok" not in _ret or not _ret["ok"]:
        logger.error(
            f"Error sending SSE message to user [{current_user.username}/{_account}], event_type={SSEDataEventType.point.value}, data={SSEDataActions.update.value}"
        )

    # if user (owner) is different than current user notify them too.
    if doc["user"] != current_user.username:
        _ret: DocInfo = await add_message(
            user=doc["user"],
            account=_account,
            event_type=SSEDataEventType.point.value,
            data=SSEDataActions.update.value,
            created_by="points.routes.update",
        )
        if not _ret or "ok" not in _ret or not _ret["ok"]:
            logger.error(
                f"Error sending SSE message to user [{current_user.username}/{_account}], event_type={SSEDataEventType.point.value}, data={SSEDataActions.update.value}"
            )

    # if status = denied, send UM.
    if doc["status"] == PointStatus.denied:
        await add_user_message(
            to_user_id=doc["user"],
            to_user_account=account,
            from_user_id=current_user.username,
            category=UserMessageCategory.points_assessment,
            priority=UserMessagePriority.high,
            subject="Points Rejected",
            message=f"Your points for {doc['point_key']} worth {(doc['amount'] * doc['quantity'])} has been denied. View your ledger for more info. The note is as follows: <br /> <pre>{doc['message']}</pre>",
            creator=current_user.username,
        )
        _ret: DocInfo = await add_message(
            user=doc["user"],
            account=_account,
            event_type=SSEDataEventType.user_message.value,
            data=SSEDataActions.new.value,
            created_by="points.routes.update",
        )
        if not _ret or "ok" not in _ret or not _ret["ok"]:
            logger.error(
                f"Error sending SSE message to user [{current_user.username}/{_account}], event_type={SSEDataEventType.user_message.value}, data={SSEDataActions.new.value}"
            )

    return await doc.info()


@points_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Hard Delete by id
    """

    _account = _id.split(":")[0]
    db = await check_access_get_db(
        account=_account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        _ret: DocInfo = await add_message(
            user=current_user.username,
            account=_account,
            event_type=SSEDataEventType.point.value,
            data=SSEDataActions.delete.value,
            created_by="points.routes.delete",
        )
        if not _ret or "ok" not in _ret or not _ret["ok"]:
            logger.error(
                f"Error sending SSE message to user [{current_user.username}/{_account}], event_type={SSEDataEventType.point.value}, data={SSEDataActions.delete.value}"
            )

    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp


@points_router.get("/totals/", response_model=PointsTotals)
async def get_total_points(
    account: str = Query(..., description="The account of the points to get"),
    username: str = Query(
        None,
        description="The username of the points to get (only applicable for admin users)",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["admins", "users"]),
):
    """
    Get total points
    - note: username is ignored if current user is not an admin
    - note: only approved points are calculated
    """
    # TODO: this should be a view

    _username = current_user.username
    if await has_role(current_user, "admins") and username:
        _username = username

    logger.debug(f"Get point totals for account={account}, username={_username}")

    # get approved points for user/account
    _points = await get_all(
        account=account,
        filter_user=_username,
        filter_status=None,
        filter_point_type=None,
        filter_point_key=None,
        filter_misc=None,
        limit=None,
        skip=0,
        current_user=current_user,
    )

    # @TODO: the following needs to be changed to a map/reduce view on the db side
    #   simple group by status with a count so we get something like:
    #     - created: 0
    #     - approved: 33
    #     - denied: 0
    points_approved = []
    points_created = []
    points_denied = []
    errors = []
    for point in _points.points:
        logger.debug(
            f">> Point type={point.point_type.value}\tamount={point.amount}\tquantity={point.quantity}\tvalue={point.amount * point.quantity}\t\tstatus={point.status.value}"
        )
        qty = point.quantity if point.quantity else 1
        amt = point.amount

        if point.status == PointStatus.approved:
            points_approved.append(amt * qty)
        elif point.status == PointStatus.created:
            points_created.append(amt * qty)
            # ignore any rewards here as unapproved throws things off.
            # ~ if point.point_type == PointType.tasks:
            # ~ points_created.append(amt * qty)
            # ~ if (amt * qty) < 1:
            # ~ logger.info(f"Calculated amount less than 1 on point={point}")
        elif point.status == PointStatus.denied:
            points_denied.append(amt * qty)
        else:
            # logger.error(f"Unknown point status: {point['status']")
            errors.append(f"Error on point: {point}")

    # return PointsTotals ==> approved, unapproved
    pt = PointsTotals()
    pt.created = sum(points_created)
    pt.approved = sum(points_approved)
    pt.denied = sum(points_denied)
    if len(errors) > 0:
        logger.error(f"Errors were found in gathering point totals: {errors}")

    return pt


@points_router.get("/leaders/", response_model=List[PointsLeaders])
async def get_leaders(
    account: str = Query(..., description="The account of the points to get"),
    current_user: UserBase = Security(get_current_user, scopes=["admins", "users"]),
):
    """
    Get points leaders
    - NOTICE: this method uses aiohttp directly as aiocouch does not currently support design docs/views.
    """

    logger.debug("- get points leaders")
    pl = []
    _url = f"{CDB_URI}/points/_design/pa/_view/total_approved"
    params = {
        "group": "true",
        # NOTE: We must include all (both) keys so we go with None⟶z) for username column
        "startkey": [account, None],
        "endkey": [account, "z"],
        "inclusive_end": "true",
        "reduce": "true",
    }

    async with aiohttp.ClientSession() as session:
        async with session.post(
            _url,
            json=params,
            auth=aiohttp.BasicAuth(CDB_USER, CDB_PASS),
            headers={"Content-Type": "application/json"},
        ) as response:
            jr = await response.json()
            logger.debug(f"- response is: {jr}")
            for leader in jr["rows"]:
                logger.debug(f"- leader: {leader} ⟶ [{account}]=[{leader['key'][0]}]")
                lo = PointsLeaders()
                lo.username = leader["key"][1]
                lo.amount = round(leader["value"], 2)
                pl.append(lo)

    return pl


@points_router.get("/accounting_totals/", response_model=object)
async def get_accounting_totals(
    account: str = Query(..., description="The account of the points to get"),
    username: str = Query(None, description="The username of the point (only applicable for admin users)"),
    current_user: UserBase = Security(get_current_user, scopes=["admins", "users"]),
):
    """
    Get points accounting totals
    """

    return await get_account_totals(account=account, username=username)
