#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from enum import Enum
from typing import List
from uuid import UUID

from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.POINTS


class PointStatus(str, Enum):
    created = "created"
    approved = "approved"
    denied = "denied"
    deleted = "deleted"


class PointsTotals(BaseModel):
    created: float = 0
    approved: float = 0
    denied: float = 0


class PointsLeaders(BaseModel):
    username: str = 0
    amount: float = 0


class PointType(str, Enum):
    tasks = "tasks"
    rewards = "rewards"


class PointBase(BaseModel):
    """
    Base
    """

    account: str = None
    user: str = None
    amount: float = 0
    status: PointStatus = PointStatus.created
    point_type: PointType = None
    point_key: str = None
    note: str | None = None
    message: str | None = None
    quantity: float = 1
    tid: UUID | None = (
        None  # TransactionID is a guid used to relate two or more transactions (i.e. send points service)
    )


class PointExt(PointBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: str | None = None
    updator: str | None = None


class Point(PointExt):
    """
    Actual (at DB level)
    """

    id_: str


class PointsResponse(BaseModel):
    """
    Points Response
    """

    total: int | None = None
    points: List[Point] | None = None
