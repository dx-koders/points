#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com
#
# LOGGING: designed to run at INFO loglevel.

import logging

from _common.utils import check_databases
from accounts.routes import accounts_router
from auth.routes import auth_router
from config import config
from config.config import cfg
from core.routes import core_router
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from rewards.routes import rewards_router
from sse.routes import sse_router
from stashes.routes import stashes_router
from tasks.routes import tasks_router
from user_messages.routes import user_messages_router
from users.routes import users_router
from contextlib import asynccontextmanager
from points.routes import points_router

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(config.APP_LOGLEVEL))
console_handler = logging.StreamHandler()
if "console" in config.LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": cfg.core.name, "application_env": config.DEPLOY_ENV},
)

logger.info(f"{cfg.core.name} - v.{cfg.core.version} ({cfg.core.modified}) - {config.APP_LOGLEVEL} - {config.LOG_TO}")

# log startup items
_lsi = []
_lsi.append(("Log Level", config.APP_LOGLEVEL))
_lsi.append(("Log To", config.LOG_TO))
_lsi.append(("Deploy Env", config.DEPLOY_ENV))
_lsi.append(("CDB URI", config.CDB_URI))
_lsi.append(("API Path", config.API_PATH))
_lsi.append(("Auth URL", f"{config.LDAP_HOST}:{config.LDAP_PORT}"))
_lsi.append(("CORS Origins", config.CORS_ORIGINS))
_lsi.append(
    (
        "JWT Expire",
        f"{config.API_JWT_EXPIRE_DAYS}:{config.API_JWT_EXPIRE_HOURS}:{config.API_JWT_EXPIRE_MINUTES} (Days:Hours:Minutes)",
    )
)
_lsi.append(
    (
        "LDAP",
        f"{config.LDAP_HOST}:{config.LDAP_PORT} {'(using ssl)' if config.LDAP_USE_SSL else ''}",
    )
)

# find longest key length and add 4
lsi_lk_length = len(sorted(_lsi, key=lambda x: len(x[0]), reverse=True)[0][0]) + 4
# sort items by key
_lsi.sort(key=lambda a: a[0])
for item in _lsi:
    logger.info(f"  ✦ {item[0].ljust(lsi_lk_length, '.')}: {item[1]}")


@asynccontextmanager
async def lifespan(app: FastAPI):
    # ensure db is setup properly.
    await config.couchdb.check_credentials()
    await check_databases()
    yield
    # close db connection.
    await config.couchdb.close()


app = FastAPI(
    title=f"{cfg.core.name}",
    description=f"{cfg.core.description}",
    version=f"{cfg.core.version}",
    openapi_url=f"{config.API_PATH}/openapi.json",
    docs_url=f"{config.API_PATH}/docs",
    redoc_url=None,
    lifespan=lifespan,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=config.CORS_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    accounts_router,
    prefix=f"{config.API_PATH}/accounts",
    tags=["accounts"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    points_router,
    prefix=f"{config.API_PATH}/points",
    tags=["points"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    stashes_router,
    prefix=f"{config.API_PATH}/stashes",
    tags=["stashes"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    tasks_router,
    prefix=f"{config.API_PATH}/tasks",
    tags=["tasks"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    rewards_router,
    prefix=f"{config.API_PATH}/rewards",
    tags=["rewards"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    users_router,
    prefix=f"{config.API_PATH}/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    user_messages_router,
    prefix=f"{config.API_PATH}/user_messages",
    tags=["user_messages"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    sse_router,
    prefix=f"{config.API_PATH}/sse",
    tags=["sse"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    core_router,
    prefix=f"{config.API_BASE_PATH}",
    tags=["core"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    auth_router,
    prefix=f"{config.API_PATH}/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)
