#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import inspect
import logging
from datetime import datetime
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, get_route_roles, make_id
from accounts.utils import check_access_get_db
from fastapi import APIRouter, Body, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from tasks.models import Kind, Task, TaskExt, TaskGroup, _db
from users.models import UserBase, get_current_user
from sse.models import SSEDataEventType, SSEDataActions
from sse.utils import add_message

logger = logging.getLogger("default")
tasks_router = APIRouter()


async def _get_or_404(_id, db, _all: bool = False):
    """
    Get a document or raise a 404
    """

    # @TODO: I think we need to change this to get_or_none and update logic accordingly.
    #   ^-- dont want to do this if we dont have to...
    # ~ logger.debug(f"_get_or_404 request on: {_id} with all: {_all}")
    try:
        doc = await db[_id]
        if _all is False and not doc["enabled"]:
            raise HTTPException(status_code=404, detail="Not found")
        return doc

    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")
        # ~ logger.debug(f"- _get_or_404 didn't find anything, returning None")
        # ~ return None


@tasks_router.get("/{_id}", response_model=Task)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    logger.debug("- tasks get_one...")

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    doc = Task(**fix_id(await _get_or_404(_id, db)))
    logger.debug(f"- get_one returning doc: {doc}")

    return doc


@tasks_router.get("/", response_model=List[Task])
async def get_all(
    account: str,
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all for account
    - disabled and system tasks are not shown unless user is admin
    """

    # @TODO: add limit and skip logic.
    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    is_admin = "admins" in current_user.roles

    # get for given account (only include enabled and available_units != 0)
    selector = {
        "account": account,
        "enabled": True,
        "$or": [
            {"available_units": {"$ne": 0}},
            {"available_units": {"$exists": False}},
        ],
    }
    # if user is admin they get disabled and 0 unit tasks to be able to edit them.
    if is_admin:
        selector = {"account": account}
    else:
        # NOTICE: here, needs to be reworked to filter all but system (including null)
        selector["$or"] = [{"kind": {"$ne": "system"}}, {"kind": {"$exists": False}}]

    docs = []
    async for doc in db.find(selector=selector):
        docs.append(Task(**fix_id(doc)))

    return docs


@tasks_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    account: str = Query(..., description="The account of the task"),
    name: str = Query(..., description="The name of the task"),
    description: str = Query(None, description="A description of the task"),
    value: float = Query(..., description="The value of the task"),
    icon: str = Query(None, description="The icon for the task"),
    enabled: bool = Query(True, description="Is the task enabled?"),
    available_units: int = Query(None, description="Number of units available (leave blank for unlimited)"),
    kind: Kind = Query(Kind.general, description="The kind of task"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    """

    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    task = TaskExt(
        account=account,
        name=name,
        value=value,
        enabled=enabled,
        description=description,
        icon=icon,
        available_units=available_units,
        kind=kind,
        created=datetime.now(),
        updated=datetime.now(),
    )

    try:
        doc = await db.create(make_id(items=[account, name]), data=jsonable_encoder(task))
        await doc.save()
        _ret: DocInfo = await add_message(
            user=current_user.username,
            account=account,
            event_type=SSEDataEventType.task.value,
            data=SSEDataActions.add.value,
            created_by="tasks.routes.add",
        )
        if not _ret or "ok" not in _ret or not _ret["ok"]:
            logger.error(
                f"Error sending SSE message to user [{current_user.username}/{account}], event_type={SSEDataEventType.task.value}, data={SSEDataActions.add.value}"
            )

        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{name}'.")


@tasks_router.put("/", response_model=DocInfo)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    # ~ account: str = Query(..., description="account of document to update"),
    name: str = Query(None, description="Name of task"),
    description: str = Query(None, description="A description of the task"),
    value: float = Query(None, description="The value of the task"),
    icon: str = Query(None, description="The icon for the task"),
    enabled: bool = Query(True, description="Is the task enabled?"),
    available_units: int = Query(None, description="Number of units available (leave blank for unlimited)"),
    kind: Kind = Query(Kind.general, description="The kind of task"),
    groups: List[TaskGroup] = Body([], description="groups the task belongs to"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update
    - NOTICE: account will not be updated, it is used for SSE notification only
    """

    _account = _id.split(":")[0]
    db = await check_access_get_db(
        account=_account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    doc = await _get_or_404(_id, db, True)
    doc_groups = doc["groups"] if "groups" in doc else []
    doc["name"] = name if name else doc["name"]
    doc["value"] = value if value else doc["value"]
    doc["icon"] = icon if icon else doc["icon"]
    doc["description"] = description if description else doc["description"]
    doc["enabled"] = enabled
    doc["available_units"] = available_units
    doc["kind"] = kind if kind else doc["kind"]
    doc["groups"] = jsonable_encoder(groups) if len(groups) > 0 else doc_groups

    doc["updated"] = jsonable_encoder(datetime.now())
    await doc.save()
    _ret: DocInfo = await add_message(
        user=current_user.username,
        account=_account,
        event_type=SSEDataEventType.task.value,
        data=SSEDataActions.update.value,
        created_by="tasks.routes.update",
    )
    if not _ret or "ok" not in _ret or not _ret["ok"]:
        logger.error(
            f"Error sending SSE message to user [{current_user.username}/{_account}], event_type={SSEDataEventType.task.value}, data={SSEDataActions.update.value}"
        )

    return await doc.info()


@tasks_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str = Query(..., description="the id of the task to delete"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Hard Delete by id
    """

    _account = _id.split(":")[0]
    db = await check_access_get_db(
        account=_account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db, _all=True)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        _ret: DocInfo = await add_message(
            user=current_user.username,
            account=_account,
            event_type=SSEDataEventType.task.value,
            data=SSEDataActions.delete.value,
            created_by="tasks.routes.delete",
        )
        if not _ret or "ok" not in _ret or not _ret["ok"]:
            logger.error(
                f"Error sending SSE message to user [{current_user.username}/{_account}], event_type={SSEDataEventType.task.value}, data={SSEDataActions.delete.value}"
            )
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
