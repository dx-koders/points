#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from enum import Enum
from typing import List, Optional

from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.TASKS


class TaskGroup(BaseModel):
    label: str = None
    priority: int = 99


class Kind(str, Enum):
    general = "general"
    system = "system"


class TaskBase(BaseModel):
    """
    Base
    """

    account: str = None  # id of the account to which item belongs
    name: str = None
    value: float = 1
    enabled: bool = True
    description: Optional[str] = None
    icon: Optional[str] = None
    usage_count: Optional[int] = 0
    available_units: Optional[int] = None
    groups: Optional[List[TaskGroup]] = []
    kind: Kind = Kind.general


class TaskExt(TaskBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()


class Task(TaskExt):
    """
    Actual (at DB level)
    """

    id_: str
