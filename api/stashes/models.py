#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from enum import Enum
from typing import Optional

from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.STASHES


class StashEditType(str, Enum):
    edit = "edit"
    deduct = "deduct"


class StashBase(BaseModel):
    """
    the base
    """

    account: str = None
    user: str = None
    icon: Optional[str] = None
    label: str = None
    amount: float = 0
    note: Optional[str] = None


class StashExt(StashBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: Optional[str] = None
    updator: Optional[str] = None


class Stash(StashExt):
    """
    Actual (at DB level)
    """

    id_: str


class StashResponse(Stash):
    """
    Stash respone.
    """

    # is_general: bool = False
