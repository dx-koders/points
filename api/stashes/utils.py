#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging

logger = logging.getLogger("default")


# _stashes_balance = await get_balance(db=db, account=account, user=username)
async def get_balance(db=None, account=None, user=None):
    """
    Get stash balance for account/user.
    """

    selector = {"_id": {"$regex": f"^{account}:{user}.*$"}}
    logger.debug(f"- get_all selector: {selector} (with account={account}/user={user})")
    amounts = []
    async for doc in db.find(selector=selector):
        amounts.append(doc["amount"])

    logger.debug(f"- stashes amounts={amounts}")

    return sum(amounts)
