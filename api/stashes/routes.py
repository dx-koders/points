#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import inspect
import logging
from datetime import datetime
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, get_route_roles, make_id
from accounts.utils import check_access_get_db
from auth.utils import has_role
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from stashes.models import Stash, StashEditType, StashExt, StashResponse, _db
from stashes.utils import get_balance
from users.models import UserBase, get_current_user
from users.utils import determine_username

from points.utils import get_account_totals

logger = logging.getLogger("default")
stashes_router = APIRouter()


async def _get_or_404(_id, db, _all: bool = False):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id} with all: {_all}")
    try:
        doc = await db[_id]
        # note: _all is ignored as we do not currently have enabled/disabled for stashes.
        # ~ if _all is False and not doc["enabled"]:
        # ~ raise HTTPException(status_code=404, detail="Not found")
        return doc

    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@stashes_router.get("/all", response_model=List[StashResponse])
async def get_all(
    account: str = Query(..., description="The account of the point"),
    username: str = Query(None, description="The username of the point (only applicable for admin users)"),
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all for account/user
    - username is only applicable for users who are admin
    """

    logger.debug("- stashes get_all...")
    # @TODO: add limit and skip logic.
    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    _username = await determine_username(current_user=current_user, username=username)
    user = current_user.username
    # allow overriding if admin
    if await has_role(current_user, "admins"):
        if username:
            user = username
        else:
            # used to get 'all'
            user = ""

    # get all stashes for given account.
    selector = {"_id": {"$regex": f"^{account}:{user}.*$"}}
    # ~ logger.debug(f"- get_all selector: {selector} (with username: {username})")
    docs = []
    async for doc in db.find(selector=selector):
        docs.append(StashResponse(**fix_id(doc)))

    # add the GENERAL stash which is a virtual stash showing remaining balance.
    _account_totals = await get_account_totals(account=account, username=_username)
    # ~ logger.debug(f"- account totals: {_account_totals}")
    _account_balance = _account_totals["debits"] - _account_totals["credits"]
    _stashes_balance = await get_balance(db=db, account=account, user=_username)
    _amount = _account_balance - _stashes_balance

    docs.append(
        StashResponse(
            id_="BAL",
            label="GENERAL",
            amount=_amount,
            note="Calculated - remaining points not allocated to a stash",
        )
    )

    # logger.debug(f"- returning stashes of: {docs}")
    return docs


@stashes_router.get("/one/{_id}", response_model=Stash)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    return Stash(**fix_id(await _get_or_404(_id, db)))


@stashes_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    account: str = Query(..., description="The account of the stash"),
    username: str = Query(None, description="The username of the stash (only applicable for admin users)"),
    icon: str = Query(None, description="Icon for the stash"),
    label: str = Query(..., description="Label for the stash"),
    amount: float = Query(..., description="Amount of the stash"),
    note: str = Query(None, description="A note of the stash"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    - note: if current user is not admin the username is ignored
    """

    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )
    _username = await determine_username(current_user=current_user, username=username)
    logger.debug(f"- adding with username=[{_username}]")

    # check to ensure user has available funds to cover the stash
    _account_totals = await get_account_totals(account=account, username=_username)
    logger.debug(f"account totals: {_account_totals}")
    _account_balance = _account_totals["debits"] - _account_totals["credits"]
    _stashes_balance = await get_balance(db=db, account=account, user=_username)
    logger.debug(f"stashes balance: {_stashes_balance}")
    if _stashes_balance + amount > _account_balance:
        raise HTTPException(
            status_code=417,
            detail=f"Insufficient funds available for request.<br/> - Account Balance: {_account_balance}<br/>- Stashes Balance: {_stashes_balance}<br/>- Available for Stash: {_account_balance - _stashes_balance}<br/>- Requested Amount: {amount}",
        )

    stash = StashExt(
        account=account,
        user=_username,
        icon=icon,
        label=label,
        amount=amount,
        note=note,
        created=datetime.now(),
        updated=datetime.now(),
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[account, _username, label])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(stash),
        )
        await doc.save()
        r = await doc.info()
        return r

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{account}/{username}'.")


@stashes_router.put("/", response_model=DocInfo)
async def update(
    _id: str,
    # account: str = Query(..., description="The account of the stash"),
    # username: str = Query(
    #    None, description="The username of the stash (only applicable for admin users)"
    # ),
    icon: str = Query(None, description="Icon for the stash"),
    label: str = Query(None, description="Label for the stash"),
    amount: float = Query(None, description="Amount of the stash"),
    note: str = Query(None, description="A note of the stash"),
    edit_type: StashEditType = Query(StashEditType.edit, description="Edit type"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update
    NOTE: update to the 'GENERAL' bucket is ignored as this is a 'virtual' stash (calculated).
    """
    # @TODO currently dont allow update of account and username for now (need to update pk)

    # ignore edit if on 'GENERAL' (virtual) stash.
    if _id == "BAL":
        return DocInfo(
            ok=True,
            id="BAL",
            rev="BAL",
            msg="Virtual stash ignored, no action performed",
        )

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    doc = await _get_or_404(_id, db)
    logger.debug(f"doc is: {doc}")

    _old_amount = doc["amount"]
    # if this is a deduct, subtract amount from old amount.
    _amount = amount
    if edit_type == StashEditType.deduct and amount:
        _amount = _old_amount - amount
    logger.debug(f"- updating with amount={_amount}")

    # check to ensure user has available funds to cover the stash
    account = doc["account"]
    user = doc["user"]

    _account_totals = await get_account_totals(account=account, username=user)
    # ~ logger.debug(f"- account totals: {_account_totals}")
    _account_balance = _account_totals["debits"] - _account_totals["credits"]
    _stashes_balance = await get_balance(db=db, account=account, user=user)
    if _stashes_balance - _old_amount + _amount > _account_balance:
        raise HTTPException(
            status_code=417,
            detail=f"Insufficient funds available for request.<br/> - Account Balance: {_account_balance}<br/>- Stashes Balance: {_stashes_balance}<br/>- Available for Stash: {_account_balance - _stashes_balance}<br/>- Requested Amount: {amount}",
        )

    doc["icon"] = icon if icon else doc["icon"]
    doc["label"] = label if label else doc["label"]
    doc["amount"] = _amount if _amount else doc["amount"]
    doc["note"] = note if note else doc["note"]
    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    logger.debug(f"doc before save: {doc}")
    await doc.save()

    return await doc.info()


@stashes_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str = Query(..., description="the id of the stash to delete"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Hard Delete by id
    """

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db, _all=True)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
