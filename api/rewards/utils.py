#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

from rewards.models import _db
from rewards.routes import _get_or_404
from config.config import couchdb


async def reward_usage_adjustment(
    _id: str = None,
    available_units: int = None,
):
    """
    Usage adjustment updates the usage_count and/or available units of a Task.
    - note: this decreases the reward.available_units if applicable
    """

    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)

    if available_units:
        doc["available_units"] = available_units

    await doc.save()
