#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from enum import Enum
from typing import Optional

from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.REWARDS


class Kind(str, Enum):
    general = "general"
    system = "system"


class RewardBase(BaseModel):
    """
    Base
    """

    account: str = None  # id of the account to which item belongs
    name: str = None
    value: float = 1
    description: Optional[str] = None
    enabled: bool = True
    icon: Optional[str] = None
    available_units: Optional[int] = None
    kind: Kind = Kind.general


class RewardExt(RewardBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()


class Reward(RewardExt):
    """
    Actual (at DB level)
    """

    id_: str
