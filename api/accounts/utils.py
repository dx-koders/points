#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from accounts.models import Account
from auth.utils import has_account_access
from config.config import couchdb
from fastapi import HTTPException
from users.models import UserBase

logger = logging.getLogger("default")


async def check_access_get_db(account: str = "", current_user: UserBase = "", roles: list = [], db=None):
    """
    Check access and return appropriate db
    Return: tasks db object
    """

    logger.debug(
        f"checking access & get db for:\n  + account={account}\n  + current user: {current_user}\n  + route roles: {roles}\n  + db: {db.value}"
    )
    acc = await get_account(account)
    logger.debug(f"- found account: {acc}")
    if not await has_account_access(current_user=current_user, account=acc, roles=roles):
        raise HTTPException(status_code=401, detail="Not authorized")

    return await couchdb[db.value]


async def get_account(name: str = None) -> Account:
    """
    Get account by name
    """

    from accounts.routes import _get_or_404

    logger.debug(f"getting account of name: {name}")
    doc = await _get_or_404(name)
    logger.debug(f"accounts.utils.get_account - doc: {doc}")
    return Account(**doc)
