#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com


from datetime import datetime
from typing import Optional

from config.models import Cdb
from pydantic import BaseModel

_db = Cdb.ACCOUNTS


class AccountBase(BaseModel):
    """Base"""

    name: str = None
    note: Optional[str] = None

    enabled: bool = True


class AccountExt(AccountBase):
    """Extended (added by backend logic)"""

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()


class Account(AccountExt):
    """Actual (at DB level)"""

    id_: str
