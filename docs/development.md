# DEVELOPMENT

Information about development for this project.

##  Developing  ##

###  Pre-Commit Info  ###

- exclude line for detect-secrets: `# pragma: allowlist secret`

#### Setup ####

- install pre-commit: `pip install pre-commit`
- init pre-commit for project: `pre-commit install`
- use: perform a commit!


##  Code Wrapup  ##
After coding is complete there are a few general tasks which should be performed:

- run bandit (currently not in pre-commit hook as it has issues with black)
- ensure the following files are synced:
    - Dockerfile*
    - docker-compose*
    - .env*
    - configmap.yml*
    - deployment-*
    - no sensitive information left in code, examples, or docs
    - increment app version accordingly

##  Releases  ##

K8s releases are rolled out (we use rancher but any mechanism will work) from the built image.


## Tips and Useful Info ##

- scale number of api servers in docker-compose: `docker-compose up --scale api=2 api`
- need something to add the db indexes.
- manually cancel a scheduled task:
```
# connect to container
python3   # get python shell
import redis
from rq import Queue
from rq_scheduler import Scheduler
import config as Config
rcon = redis.from_url(Config.DefaultConfig.REDIS_URL)
scheduler = Scheduler(connection=rcon)

# remove item from scheduler
key = '1e698bd8-5064-4eed-bd50-5139c5feae3e'
scheduler.cancel(key)

# check the failed job registry
queue = Queue(connection=rcon)
registry = queue.failed_job_registry
len(registry)
```
