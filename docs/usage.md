# Usage


### Roles

Points has 3 types of roles, currently all handled by the LDAP auth mechanism:

- points_admins: admins have full permission
- points_managers: managers have permission to Edit (not Add or Delete) Tasks and Rewards
- points_users: users have basic permissions that allow using the application to Submit Tasks and Rewards for approval by admins but they cannot Add, Update or Delete Tasks or Rewards

You can assign the any/all roles to a user in LDAP by making them a member of the corresponding group. An example in Glauth would be as follows:

```
...
# specify the user and add (in otherGroups) the group(s) you want them to belong to:
[[users]]
  name = "drad"
  uidnumber = 7000
  primarygroup = 8000   # app
  disabled = false
  otherGroups = [9000, 9001, 9010, 9011, 9020, 9021, 9030, 9031, 9040, 9041, 9050, 9051, 9060, 9061]

...
# specify the group (noting that the user above is a member of 9010 (points_admins) and 9011 (points_users))
[[groups]]
  name = "points_admins"
  gidnumber = 9010
[[groups]]
  name = "points_users"
  gidnumber = 9011
[[groups]]
  name = "points_managers"
  gidnumber = 9012
...
```

Also note that you must give the user the application 'manager' role. This is a setting on the User for the account in question, for example:

```
{
  "_id": "addalie",
  "username": "addalie",
  "roles": [
    "users"
  ],
  "accounts": [
    {
      "id_": "default",
      "role": "manager"
    }
  ],
  "enabled": true,
```
Notice the "role": "manager" for account "default", this (combined with the LDAP role) gives the user manager access in the application.
