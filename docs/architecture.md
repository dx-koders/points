# Architecture

###  Stack  ###

The application stack is as follows:
- api: backend application
    - fastapi: core application
    - uvicorn/gunicorn: http/wsgi interface to application
    - ldap3: ldap authentication
- db: couchdb
- redis: currently not used but may be at a later point for caching


###  Authorization Scheme  ###

See the authorization_scheme.ods spreadsheet for details related to authorization including a breakout of privileges by Method for each Model in the application.

###  Links  ###
- [apscheduler](https://apscheduler.readthedocs.io)
- [arq](https://arq-docs.helpmanual.io/)
- [fastapi](https://fastapi.tiangolo.com/)


###  Websockets vs Polling  ###

We have investigated websockets vs polling for points, tasks, rewards, etc. and found it to not be viable at this time. See the `README.md` in the `investigate/websockets` for more details.
