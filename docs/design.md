# design

### Points ###

- no support for group tasks / group rewards:
  + no group tasks/rewards - all will be individual based.
    * tasks: "Chop down tree" for 3 points for User A and User B - this must rather be setup as "Chop down tree" for 1.5 points. User A will submit the task for 1.5 points and User B will also submit for 1.5 points for a total of 3 points.
    * rewards "WiEat" for 3 points for User A, User B, User C - this should rather be "WiEat" for 1 point per user.
