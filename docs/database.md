# database

- make sure the `db/data` dir and all below it are owned by uid=1001: `sudo chown -R 1001:1001 db/data/`
- you can do most everything from its [admin interface](http://localhost:5984/_utils)
