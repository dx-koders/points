
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Welcome.vue') },
      { path: '/news', component: () => import('pages/News.vue') },
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/help', component: () => import('pages/Help.vue') },
      { path: '/about', component: () => import('pages/About.vue') },
      { path: '/points', component: () => import('pages/Points.vue') },
      { path: '/tasks', component: () => import('pages/Tasks.vue') },
      { path: '/rewards', component: () => import('pages/Rewards.vue') },

      { path: '/edit_profile', component: () => import('pages/EditProfile.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
