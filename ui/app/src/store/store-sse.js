/*
 * SSE: handles SSE related items.
 */

import VueSSE from 'vue-sse';
//~ import user from './store-user'


const state = {
  loading: false,
  sseClient: {},
}

const mutations = {
  setLoading(state, payload) {
    state.loading = payload
  },
  setSseClient(state, payload) {
    state.sseClient = payload
  },
  disconnectSseClient(state, payload) {
    state.sseClient.disconnect()
  },
}

const actions = {
  setSseClient( { commit }, payload) {
    //commit('setLoading', true)
    //set sseClient.
    commit('setSseClient', payload)
  },
  disconnectSseClient( { commit }, payload) {
    commit('disconnectSseClient', payload)
  },
  setupStream({ commit, dispatch, rootState, rootGetters }, payload) {
    console.log("Setting up stream with payload:", payload);
    let sse_url = process.env.UI_SSE_URL + payload.notify_id + ":" + payload.account;
    console.log("SSE Stream URL: " + sse_url);
    payload.sseClient = payload.sse.create(sse_url)
      /*
       * NOTICE: on 2024-08-23 we removed all redis logic for SSE and change the structure of the Event being sent back
       * to use the Type as the type of event (e.g. user_message, task, etc.) rather than pass back a generic 'message'
       * and then parse it on the UI to see what type of message was sent. Before we had a listener as follows:
       *   .on('message', (msg) => console.info('Message:', msg))
       * Now we have a listener as follows:
       *   .on('{Event}', (data) => {})
       */
      .on('user_message', (data) => {
        const actions = ['new']
        //console.log("user_message event received, data=[" + data + "]");
        if ( actions.includes(data) ) {
          //console.log("- new user_messages exist, need to fetch...");
          dispatch("user_messages/fetchUserMessages", null, { root: true });
        } else {
          console.error("Unknown/unhandled data for user_message, data=[" + data + "]");
        }
      })
      .on('task', (data) => {
        const actions = ['add', 'update', 'delete']
        if ( actions.includes(data) ) {
          //console.log("Need to fetchTasks...");
          //this.fetchTasks()
          dispatch("tasks/fetchTasks", null, { root: true });
        } else {
          console.error("Unknown/unhandled data for task, data=[" + data + "]");
        }
      })
      .on('reward', (data) => {
        const actions = ['add', 'update', 'delete']
        if ( actions.includes(data) ) {
          //console.log("Need to fetchRewards...");
          //this.fetchRewards()
          dispatch("rewards/fetchRewards", null, { root: true });
        } else {
          console.error("Unknown/unhandled data for reward, data=[" + data + "]");
        }
      })
      .on('point', (data) => {
        const actions = ['add', 'update', 'delete']
        if ( actions.includes(data) ) {
          let user = rootGetters['user/user'];
          //console.log("Need to fetchPoints...");
          dispatch("points/fetchPoints", {account: user.active_account_name }, { root: true });
          //console.log("Need to fetchUserPoints...");
          dispatch("user/fetchUserPoints", null, { root: true });
          //console.log("Need to fetchLeaders...");
          dispatch("points/fetchLeaders", {account: user.active_account_name}, { root: true });
          //console.log("Need to fetchAccountingTotals...");
          dispatch("points/fetchAccountingTotals", {account: user.active_account_name, username: user.username}, { root: true });
        } else {
          console.error("Unknown/unhandled data for point, data=[" + data + "]");
        }
      })
      .on('error', (err) => {
        // handle connection error which happens if BE is reloaded or there are network issues.
        console.log('Failed to parse or lost connection:', err)
        payload.sseClient.connect();
      })
      .connect()
        .then(sse => {
          console.log('⚓ SSE Stream connected: ' + sse_url);
        })
      .catch((err) => console.error('Failed to make initial connection:', err));
  },
}

const getters = {
    loading: (state) => {
      return state.loading
    },
    sseClient: (state) => {
      return state.sseClient
    },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
