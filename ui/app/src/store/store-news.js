const state = {
    /*
     * icons:
     *   - new_releases: general new stuff
     *   - announcement: lower priority items
     */
    items: [
        {date: '2021-08-26', title: 'Happy Birthday Mein & Chunk!!', icon: 'cake', detail: 'Natalie and Addalie turn 8 on 2021-08-27, seems like it was yesterday when you two were making poop faster than I could change it ;-)'},
        //{date: '', title: '', icon: 'new_releases', detail: ''},
    ]
}

const mutations = {}

const actions = {}

const getters = {
    items: (state) => {
        return state.items
    },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
