import Vue from 'vue'
import { api } from 'boot/axios'

const state = {
    points_loading: true,
    points: [],
    points_count: {
        total: 0
    },
    setAccountingLoading: true,
    accounting: {
        debits: 0,
        credits: 0,
    },
    leaders: [],
}

/* Mutations are for changes to the state, api calls should be in actions and use mutation to reflect api info.
   NOTICE: no async in mutations
*/
const mutations = {
    setPointsLoading(state, payload) {
        state.points_loading = payload
    },
    addPoints(state, payload) {
        state.points = payload
    },
    setPointsCount(state, payload) {
        state.points_count.total = payload
    },
    updatePoint(state, payload) {
        //console.log("store-points>mutations>updatePoint - payload: ", payload)
        // note: we are not updating the store here as we update the db and reload
        /*
         * NOTICE: we do not allow updating the point_type/point_key as the
         *   point values could change which causes a lot of issues. Rather
         *   than allow updates of these fields we deny the point and have
         *   the user add a new one.
         */

        var querystring = require('querystring');
        api.put('/points/?' + querystring.stringify(payload))
            .then((response) => {
                //console.log("point update response: ", response)
            })
        .catch((error) => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            //~ console.log('No points found.')
            console.log("Error approving point: ", error)
        })
    },
    deletePoint(state, id_) {
        var querystring = require('querystring');
        //~ console.log("store-points>mutations>deletePoint id: ", id_)
        // delete from db
        api.delete('/points/?' + querystring.stringify({ _id: id_ }), { params: {account: "default"}})
            .then((response) => {
                //~ console.log('fetch points returned: ', JSON.stringify(response.data))
                //this.points = response.data
                //console.log("point deleted with id: ", id_)
            })
        .catch((error) => {
            //       no harm in the message so we just dump to console.
            //~ console.log('No points found.')
            console.log("Error in deleting point: ", error)
        })
        // if db update all good, update store.

        // you need to get the index of the item to delete it from vue
        // NOTE: I'm not sure this is a worthwhile effort, its likely better (although slightly slower)
        //   to simply reload points from db (fire the event) than to worry about this.
        //~ var index = state.points.findIndex(point => point.id_ == id_);
        //~ console.log(" --> index is: ", index)
        //~ Vue.delete(state.points, index)
    },
    setAccountingLoading(state, payload) {
        state.points_loading = payload
    },
    setAccountings(state, payload) {
        state.accounting = payload
    },
    addLeaders(state, payload) {
        state.leaders = payload
    },
}

/*
 * Actions are for api work, use mutations to reflect changes in state.
 */
const actions = {
    fetchPoints({ commit }, payload) {
        commit('setPointsLoading', true)
        //~ console.log("vuex>store-points: fetchPoints with payload::", payload)
        api.get('/points/all', { params: payload})
            .then((response) => {
                //~ console.log('fetch points returned: ', JSON.stringify(response.data))
                //~ for (point in JSON.stringify(response.data)) {
                    //~ console.log("- point: ")
                //~ }
                commit('addPoints', response.data.points)
                commit('setPointsCount', response.data.total)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No points found.')
            })
        commit('setPointsLoading', false)
    },
    fetchAccountingTotals({ commit }, payload) {
        commit('setAccountingLoading', true)
        api.get('/points/accounting_totals/', { params: payload})
            .then((response) => {
                //console.log('fetch accounting totals returned: ', JSON.stringify(response.data))
                commit('setAccountings', response.data)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No accounting totals found!')
            })
        commit('setAccountingLoading', false)
    },
    addPoint({ commit }, point) {
        //console.log("--> SENDING POINT:", point);
        return new Promise((resolve, reject) => {
            api.post('/points/', null, { params: point})
            .then((response) => {
                // no state to change as we fetch later to get all
                resolve(true)
            })
            .catch((error) => {
                reject(error)
            })
        })

    },
    updatePoint({ commit }, payload) {
        //~ console.log("store-points>action>updatePoint - payload: ", payload)
        commit('updatePoint', payload)
    },
    deletePoint({ commit }, id_) {
        //~ console.log("store-points>action>deletePoint with id: ", id)
        commit('deletePoint', id_)
    },
    fetchLeaders({ commit }, payload) {
        //~ console.log("store > points-leaders > fetch")
        api.get('/points/leaders/', { params: payload})
            .then((response) => {
                //~ console.log('  ─⏵ fetch leaders return:', JSON.stringify(response.data))
                commit('addLeaders', response.data)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No leaders found.')
            })
    },
}

const getters = {
    points_loading: (state) => {
        return state.points_loading
    },
    points: (state) => {
        return state.points
    },
    points_count: (state) => {
        return state.points_count
    },
    accounting: (state) => {
        return state.accounting
    },
    leaders: (state) => {
        return state.leaders
    }
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
