/*
 * NOTICE: update the _version.json file for both quasar and lighttpd 'version' data.
 */

const _version = require("./_version.json")

const state = {
    ui: _version
}

const mutations = {}

const actions = {}

const getters = {
    ui: (state) => {
        return state.ui
    },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
