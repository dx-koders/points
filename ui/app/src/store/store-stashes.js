import { api } from 'boot/axios'

const state = {
    stashes_loading: true,
    stashes_total: 0,
    stashes: [],
    active_stash: {
        "account":"",
        "icon": "",
        "label":"test",
        "amount": null,
        "note": "",
    },
}

/*
 * Mutations are for changes to the state, api calls should be in actions and use mutation to reflect api info.
 * NOTICE: no async in mutations
 */
const mutations = {
    setStashesLoading(state, payload) {
        state.stashes_loading = payload
    },
    setStashes(state, payload) {
        state.stashes = payload
    },
    setActiveStash(state, payload) {
        state.active_stash = payload
    },
}

/*
 * Actions are for api work, use mutations to reflect changes in state.
 */
const actions = {
    fetchStashes({ commit }, payload) {
        return new Promise((resolve, reject) => {
            commit('setStashesLoading', true)
            //console.log("- getting stashes with payload:", payload);
            api.get('/stashes/all', { params: payload})
                .then((response) => {
                    //console.log('fetch accounting totals returned: ', JSON.stringify(response.data))
                    commit('setStashes', response.data)
                    resolve(true)
                })
                .catch((error) => {
                    // note: this is hit when an unauthenticated user hits / which redirects them to /login
                    //       no harm in the message so we just dump to console.
                    console.log('No stashes found!', error)
                    reject(error)
                })
            commit('setStashesLoading', false)
        })
    },
    addStash(state, stash) {
        //console.log("store-stashes > actions > addStash: ", stash)
        var querystring = require('querystring');
        return new Promise((resolve, reject) => {
            api.post('/stashes/?' + querystring.stringify(stash))
                .then((response) => {
                    //console.log('add stashes returned: ', JSON.stringify(response.data))
                    // no state to change as we fetch later to get all
                    resolve(true)
                })
            .catch((error) => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                //~ console.log('No tasks found.')
                //console.log("Error in adding stash: ", error)
                reject(error)
            })
        })
    },
    editStash(state, stash) {
        //console.log("store-stashes > actions > editStash: ", stash)
        var querystring = require('querystring');
        stash._id = stash.id_;
        // ensure the stash does not have undesirable properties
        delete stash.account;
        delete stash.created;
        delete stash.creator;
        delete stash.updated;
        delete stash.updator;
        delete stash.user;
        delete stash.id_;
        return new Promise((resolve, reject) => {
            api.put('/stashes/?' + querystring.stringify(stash))
                .then((response) => {
                    //console.log('add stashes returned: ', JSON.stringify(response.data))
                    // no state to change as we fetch later to get all
                    resolve(true)
                })
            .catch((error) => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                //~ console.log('No tasks found.')
                //console.log("Error in adding stash: ", error)
                reject(error)
            })
        })
    },
    deleteStash({ commit }, id) {
        //console.log("store-stashes > action > deleteStash with id: ", id)
        var querystring = require('querystring');
        return new Promise((resolve, reject) => {
            api.delete('/stashes/?' + querystring.stringify({ _id: id }))
                .then((response) => {
                    console.log('  - delete stash returned: ', JSON.stringify(response.data))
                    // no state to change as we fetch later to get all
                    resolve(true)
                })
            .catch((error) => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                //~ console.log('No tasks found.')
                console.log("Error in deleting stash: ", error)
                reject(error)
            })
        })
    },
    setActiveStash({ commit }, stash) {
        commit('setActiveStash', stash)
    },
}

const getters = {
    stashes: (state) => {
        return state.stashes
    },
    stashes_total: (state) => {
        return state.stashes_total
    },
    active_stash: (state) => {
        return state.active_stash
    },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
