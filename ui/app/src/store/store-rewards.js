import Vue from 'vue'
import { api } from 'boot/axios'

const state = {
    rewards_loading: true,
    rewards: []
    //~ rewards: [
      //~ {
        //~ "account": "default",
        //~ "name": "Dessert",
        //~ "value": 1,
        //~ "description": "Dessert after meal",
        //~ "enabled": true,
        //~ "icon": "takeout_dining",
        //~ "created": "2021-03-01T20:36:17.260109",
        //~ "updated": "2021-03-04T17:28:18.719779",
        //~ "id_": "default:dessert"
      //~ },
      //~ {
        //~ "account": "default",
        //~ "name": "Dinner + Show",
        //~ "value": 3,
        //~ "description": "Dinner + show like Beaver, Duck Dynasty, Wipeout",
        //~ "enabled": true,
        //~ "icon": "camera_indoor",
        //~ "created": "2021-03-01T20:37:31.321254",
        //~ "updated": "2021-03-04T17:23:28.847827",
        //~ "id_": "default:dinner-+-show"
      //~ }
    //~ ]

}

const mutations = {
    setRewardsLoading(state, payload) {
        state.rewards_loading = payload
    },
    addRewards(state, payload) {
        state.rewards = payload
    },
    updateReward(state, payload) {
        //~ console.log("store-rewards>mutations>updateReward payload: ", payload)
        // note: we are not updating the store here as we update the db and reload
        var querystring = require('querystring');
        api.put('/rewards/?' + querystring.stringify(payload))
            .then((response) => {
                //~ console.log("reward update response: ", response)
            })
        .catch((error) => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            console.log("Error updating reward: ", error)
        })

    },
    deleteReward(state, id_) {
        var querystring = require('querystring');
        //~ console.log("store-rewards>mutations>deleteReward id: ", id_)
        // delete from db
        api.delete('/rewards/?' + querystring.stringify({ _id: id_ }), { params: {account: "default"}})
            .then((response) => {
                //~ console.log('fetch rewards returned: ', JSON.stringify(response.data))
                //~ console.log("reward deleted with id: ", id_)
            })
        .catch((error) => {
            //       no harm in the message so we just dump to console.
            //~ console.log('No rewards found.')
            console.log("Error in deleting reward: ", error)
        })
        // if db update all good, update store.

        // you need to get the index of the item to delete it from vue
        // NOTE: I'm not sure this is a worthwhile effort, its likely better (although slightly slower)
        //   to simply reload rewards from db (fire the event) than to worry about this.
        //~ var index = state.rewards.findIndex(reward => reward.id_ == id_);
        //~ console.log(" --> index is: ", index)
        //~ Vue.delete(state.rewards, index)
    },
    addReward(state, reward) {
        //~ console.log("store-rewards>mutations>deleteReward id: ", id_)
        var querystring = require('querystring');
        api.post('/rewards/?' + querystring.stringify(reward))
            .then((response) => {
                //~ console.log('fetch rewards returned: ', JSON.stringify(response.data))
                //~ console.log("reward added: ", reward)
            })
        .catch((error) => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            //~ console.log('No rewards found.')
            console.log("Error in deleting reward: ", error)
        })
    },
    toggleRewardEnable(state, reward) {
        //~ console.log("store-rewards>mutations>toggleRewardEnable: reward=", reward)
        var querystring = require('querystring');
        let obj = {
            _id: reward.id_,
            enabled: !reward.enabled
        }
        api.put('/rewards/?' + querystring.stringify(obj))
            .then((response) => {
                //~ console.log('fetch rewards returned: ', JSON.stringify(response.data))
                //~ console.log("reward enable toggled: ", response)
            })
        .catch((error) => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            //~ console.log('No rewards found.')
            console.log("Error in deleting reward: ", error)
        })
    }
}

const actions = {
    fetchRewards({ commit }, payload) {
        commit('setRewardsLoading', true)
        //~ console.log("vuex>store-rewards: fetching rewards from server...")
        api.get('/rewards/', { params: {account: "default"}})
            .then((response) => {
                //~ console.log('fetch rewards returned: ', JSON.stringify(response.data))
                //this.rewards = response.data
                commit('addRewards', response.data)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No rewards found.')
            })
        commit('setRewardsLoading', false)
    },
    addReward({ commit }, reward) {
        commit('addReward', reward)
    },
    updateReward({ commit }, payload) {
        commit('updateReward', payload)
    },
    deleteReward({ commit }, id_) {
        commit('deleteReward', id_)
    },
    toggleRewardEnable({ commit }, enabled) {
        commit('toggleRewardEnable', enabled)
    }
}

const getters = {
    rewards_loading: (state) => {
        return state.rewards_loading
    },
    rewards: (state) => {
        return state.rewards
    }
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
